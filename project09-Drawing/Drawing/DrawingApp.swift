//
//  DrawingApp.swift
//  Drawing
//
//  Created by Michal on 02.01.2022.
//

import SwiftUI

@main
struct DrawingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
