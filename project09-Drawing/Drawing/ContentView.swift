//
//  ContentView.swift
//  Drawing
//
//  Created by Michal on 02.01.2022.
//

import SwiftUI

struct Triange: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        
        return path
    }
}

struct Arc: InsettableShape {
    let startAngle: Angle
    let endAngle: Angle
    let clockwise: Bool
    var insetAmount = 0.0
    
    init(startAngle: Angle = .degrees(0), endAngle: Angle, clockwise: Bool = true) {
        self.startAngle = startAngle - .degrees(90)
        self.endAngle = endAngle - .degrees(90)
        self.clockwise = !clockwise
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.addArc(center: CGPoint(x: rect.midY, y: rect.midY), radius: rect.width / 2 - insetAmount, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        
        return path
    }
    
    func inset(by amount: CGFloat) -> some InsettableShape {
        var arc = self
        arc.insetAmount += amount
        return arc
    }
}

struct Flower: Shape {
    var petalOffset = -20.0
    var pettalWidth = 100.0
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        for number in stride(from: 0, to: Double.pi * 2, by: Double.pi / 8) {
            let rotation = CGAffineTransform(rotationAngle: number)
            let position = rotation.concatenating(CGAffineTransform(translationX: rect.width / 2, y: rect.height / 2))
            
            let originalPetal = Path(ellipseIn: CGRect(x: petalOffset, y: 0, width: pettalWidth, height: rect.width / 2))
            
            let rotatedPetal = originalPetal.applying(position)
            
            path.addPath(rotatedPetal)
        }
        
        return path
    }
}

struct BezierFlower: View {
    // TODO: eoFill = true (make whole flower from one path with closed circles or study more of FillStyle eoFill = tue how to acomplish this)
    var petalWidth = 100.0
    var petalHeight = 200.0
    
    struct Petal: Shape {
        var petalWidth: Double
        var petalHeight: Double
        
        func path(in rect: CGRect) -> Path {
            var path = Path()
            let center = CGPoint(x: rect.midX, y: rect.midY)
            
            path.move(to: center)
            path.addCurve(to: center, control1: CGPoint(x: center.x - petalWidth, y: center.y - petalHeight), control2: CGPoint(x: center.x + petalWidth, y: center.y - petalHeight))
            
            return path
            
        }
    }
    
    var body: some View {
        ZStack {
            ForEach(Array(stride(from: 0, to: Double.pi * 2, by: Double.pi / 9)), id: \.self) { number in
                BezierFlower.Petal(petalWidth: petalWidth, petalHeight: petalHeight)
                    .stroke(.red, lineWidth: 1)
                    .rotationEffect(Angle(radians: number))
            }
        }
    }
}

struct ColorCyclingCircle: View {
    var amount = 0.0
    var steps = 100
    
    var body: some View {
        ZStack {
            ForEach(0..<steps) { value in
                Circle()
                    .inset(by: Double(value))
                    .strokeBorder(
                        LinearGradient(
                            gradient: Gradient(colors: [
                                color(for: value, brightness: 1),
                                color(for: value, brightness: 0.5),
                                color(for: value, brightness: 0),
                            ]),
                            startPoint: .top, endPoint: .bottom
                        ),
                        lineWidth: 2
                    )
            }
        }
        .drawingGroup()
    }
    
    func color(for value: Int, brightness: Double) -> Color {
        var targetHue = Double(value) / Double(steps) + amount
        
        if targetHue > 1 {
            targetHue -= 1
        }
        
        return Color(hue: targetHue, saturation: 1, brightness: brightness)
    }
}

struct ColorCyclingRectangle: View {
    var amount = 0.0
    var steps = 100
    
    var body: some View {
        ZStack {
            ForEach(0..<steps) { value in
                Rectangle()
                    .inset(by: Double(value))
                    .strokeBorder(
                        LinearGradient(
                            gradient: Gradient(colors: [
                                color(for: value, brightness: 1),
                                color(for: value, brightness: 0.5),
                                color(for: value, brightness: 0),
                            ]),
                            startPoint: .top, endPoint: .bottom
                        ),
                        lineWidth: 2
                    )
            }
        }
        .drawingGroup()
    }
    
    func color(for value: Int, brightness: Double) -> Color {
        var targetHue = Double(value) / Double(steps) + amount
        
        if targetHue > 1 {
            targetHue -= 1
        }
        
        return Color(hue: targetHue, saturation: 1, brightness: brightness)
    }
}

struct Trapezoid: Shape {
    var insetAmount: Double
    
    var animatableData: Double {
        get { insetAmount }
        set { insetAmount = newValue }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.move(to: CGPoint(x: 0, y: rect.maxY))
        path.addLine(to: CGPoint(x: insetAmount, y: 0))
        path.addLine(to: CGPoint(x: rect.maxX - insetAmount, y: 0))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: 0, y: rect.maxY))
        
        return path
    }
}

struct Checkerboard: Shape {
    var rows: Int
    var columns: Int
    
    var animatableData: AnimatablePair<Double, Double> {
        get { AnimatableData(Double(rows), Double(columns)) }
        set {
            rows = Int(newValue.first)
            columns = Int(newValue.second)
        }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let rowSize = rect.height / Double(rows)
        let columnSize = rect.width / Double(columns)
        
        for row in 0..<rows {
            for column in 0..<columns {
                if (row + column).isMultiple(of: 2) {
                    let startX = columnSize * Double(column)
                    let startY = rowSize * Double(row)
                    
                    path.addRect(CGRect(x: startX, y: startY, width: columnSize, height: rowSize))
                }
            }
        }
        
        return path
    }
}

struct Spirograph: Shape {
    let innderRadius: Int
    let outerRadius: Int
    let distance: Int
    let amount: Double
    
    func gcd(_ a: Int, _ b: Int) -> Int {
        var a = a
        var b = b
        
        while b != 0 {
            let temp = b
            b = a % b
            a = temp
        }
        
        return a
    }
    
    func path(in rect: CGRect) -> Path {
        let divisor = gcd(innderRadius, outerRadius)
        
        let outerRadius = Double(self.outerRadius)
        let innerRadius = Double(self.innderRadius)
        let distance = Double(self.distance)
        
        let difference =  innerRadius - outerRadius
        let endPoint = ceil(2 * Double.pi * outerRadius / Double(divisor)) * amount
        
        var path = Path()
        
        for theta in stride(from: 0, through: endPoint, by: 0.01) {
            var x = difference * cos(theta) + distance * cos(difference / outerRadius * theta)
            var y = difference * sin(theta) - distance * sin(difference / outerRadius * theta)
            
            x += rect.width / 2
            y += rect.height / 2
            
            if theta == 0 {
                path.move(to: CGPoint(x: x, y: y))
            } else {
                path.addLine(to: CGPoint(x: x, y: y))
            }
        }
        return path
    }
}

struct Arrow: Shape {
    
    var thickness: Double = 1/3 // 0.0 - 1.0 value
    
    var animatableData: Double {
        get { thickness }
        set {
            if newValue > 1 { thickness = 1 }
            else if newValue < 0 { thickness = 0 }
            else { thickness = newValue }
        }
    }
    
    func path(in rect: CGRect) -> Path {
        let thicknessOffset = rect.width / 2 * thickness
        
        var path = Path()
        
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        
        path.addLine(to: CGPoint(x: rect.midX + thicknessOffset, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX + thicknessOffset, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX - thicknessOffset, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX - thicknessOffset, y: rect.midY))
        
        path.addLine(to: CGPoint(x: rect.minX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        
        path.closeSubpath()
        
        return path
    }
    
    
}

struct ContentView: View {
//    @State private var innderRadius = 125.0
//    @State private var outerRadius = 75.0
//    @State private var distance = 25.0
//    @State private var amount = 1.0
//    @State private var hue = 0.6
    @State private var thickness: Double = 1/5
    
    var body: some View {
        
        VStack(spacing: 50) {
            
            ColorCyclingCircle(amount: 1, steps: 23)
                .padding()
            
            ColorCyclingRectangle(steps: 80)
                .padding()
            
        }
//        ScrollView {
//            VStack(spacing: 0) {
//                Spacer()
//
//                Spirograph(innderRadius: Int(innderRadius), outerRadius: Int(outerRadius), distance: Int(distance), amount: amount)
//                    .stroke(Color(hue: hue, saturation: 1, brightness: 1), lineWidth: 1)
//                    .frame(width: 300, height: 300)
//
//                Spacer()
//
//                Group {
//                    Text("InnerRadius: \(Int(innderRadius))")
//                    Slider(value: $innderRadius, in: 10...150, step: 1)
//                        .padding([.horizontal, .bottom])
//
//                    Text("OuterRadius: \(Int(outerRadius))")
//                    Slider(value: $outerRadius, in: 10...150, step: 1)
//                        .padding()
//
//                    Text("Distance: \(Int(distance))")
//                    Slider(value: $distance, in: 1...150, step: 1)
//                        .padding()
//
//                    Text("Amount: \(amount, format: .number.precision(.fractionLength(2)))")
//                    Slider(value: $amount)
//                        .padding([.horizontal, .bottom])
//
//                    Text("Color")
//                    Slider(value: $hue)
//                        .padding([.horizontal, .bottom])
//                }
//            }
//        }
    }
    
    func performHaptics(_ styleType: HapticsStyleType) {
        switch (styleType) {
        case .style(let style):
            let impact = UIImpactFeedbackGenerator(style: style)
            impact.impactOccurred()
        
        case .type(let type):
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(type)
        }
    }
}

enum HapticsStyleType {
    case style(UIImpactFeedbackGenerator.FeedbackStyle)
    case type(UINotificationFeedbackGenerator.FeedbackType)
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
