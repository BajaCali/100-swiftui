//
//  HabitsApp.swift
//  Habits
//
//  Created by Michal on 22.01.2022.
//

import SwiftUI

@main
struct HabitsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
