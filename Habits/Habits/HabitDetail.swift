//
//  HabitDetail.swift
//  Habits
//
//  Created by Michal on 22.01.2022.
//

import SwiftUI

struct HabitDetail: View {
    @ObservedObject var habits: Habits
    var habit: Habit
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    Text(habit.description)
                } header: {
                    Text("Description")
                }
                
                HStack {
                    Text("Complteion count: \(habit.doneTimes)")
                    Spacer()
                    Button {
                        habits.completeHabit(habit)
                    } label: {
                        Image(systemName: "plus.circle")
                    }
                }
            }
            .navigationTitle(habit.name)
        }
    }
}

struct HabitDetail_Previews: PreviewProvider {
    static var previews: some View {
        HabitDetail(habits: Habits() ,habit: .example)
    }
}
