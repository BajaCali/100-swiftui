//
//  AddHabit.swift
//  Habits
//
//  Created by Michal on 22.01.2022.
//

import SwiftUI

struct AddHabit: View {
    @Environment(\.dismiss) var dismiss
    
    @ObservedObject var habits: Habits
    
    @State var name = ""
    @State var description = ""
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Insert name of the new habit", text: $name)
                    
                } header: {
                    Text("name")
                }
            
                Section {
                    TextField("Describe the new habit here", text: $description)
                    
                } header: {
                    Text("description")
                }
            }
            .navigationTitle("Add new habit")
            .toolbar {
                ToolbarItemGroup(placement: .confirmationAction) {
                    Button("Add") {
                        do {
                            try habits.addHabit(Habit(name: name, description: description))
                            dismiss()
                        } catch HabitError.alreadyExist(let name) {
                            print("Habit called \"\(name)\" already exists")
                        }
                        catch { }
                    }
                }
                
                ToolbarItemGroup(placement: .cancellationAction) {
                    Button("Cancel", role: .destructive) {
                        dismiss()
                    }
                }
            }
        }
    }
}

struct AddHabit_Previews: PreviewProvider {
    static var previews: some View {
        AddHabit(habits: Habits())
    }
}
