//
//  Habit.swift
//  Habits
//
//  Created by Michal on 22.01.2022.
//

import Foundation

struct Habit: Identifiable, Equatable, Codable {
    let name: String
    var description: String
    var doneTimes: Int = 0
    
    var id: String { name }
    
    static var example: Habit {
        return Habit(name: "Reading", description: "This habit is about reading books. You really should do it every day to keep your bain in shape.")
    }
}
