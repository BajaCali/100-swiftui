//
//  Habits.swift
//  Habits
//
//  Created by Michal on 22.01.2022.
//

import Foundation

enum HabitError: Error {
    case alreadyExist(String)
}

class Habits: ObservableObject {
    @Published var items: [Habit] {
        didSet {
            guard let encoded = try? JSONEncoder().encode(items) else { return }
            
            UserDefaults.standard.set(encoded, forKey: "Items")
        }
    }
    
    init() {
        guard let data = UserDefaults.standard.data(forKey: "Items"),
              let decodedItems = try? JSONDecoder().decode([Habit].self, from: data) else {
                  items = [.example]
                  return
              }
        items = decodedItems
    }
    
    func addHabit(_ habit: Habit) throws -> Void {
        guard !items.contains(where: { $0.name == habit.name }) else {
            throw HabitError.alreadyExist(habit.name)
        }
        
        items.append(habit)
    }
    
    func completeHabit(_ habit: Habit) {
        guard let index = items.firstIndex(of: habit) else {
            fatalError("Habit not found")
        }
        
        items[index].doneTimes += 1
    }
}
