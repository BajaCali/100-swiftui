//
//  ContentView.swift
//  ViewsAndModifiers
//
//  Created by Michal on 13.12.2021.
//

import SwiftUI

struct ContentView: View {
    @State private var useRed = false
    
    var body: some View {
        VStack {
            VStack(spacing: 10) {
                CapsuleText(text: "Hello")
                    .foregroundColor(.white)
                Text("Goodbye")
                    .font(.caption2)
            
                CapsuleText(text: "World")
                    .foregroundColor(.yellow)
                
                Text("Text")
                    .titled()
            }
            .font(.title) // evironment modifier!
            .blur(radius: 1) // regular modifier!
            // is it envorion ment -> docs
            
            GridStack(rows: 4, columns: 4) { row, col in
                Image(systemName: "\(row * 4 + col).circle")
                Text("\(row):\(col)")
            }
        }
    }
    
    @ViewBuilder var hw: some View {
            Text("Hello")
            Text("World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CapsuleText: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.largeTitle)
            .padding()
            .background(.blue)
            .clipShape(Capsule())
    }
}

struct Title: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(.white)
            .padding()
            .background(.blue)
            .clipShape(RoundedRectangle(cornerRadius: 10))
    }
}

extension View {
    func titled() -> some View {
        modifier(Title())
    }
}


struct GridStack<Content: View>: View {
    let rows: Int
    let columns: Int
    @ViewBuilder let content: (Int, Int) -> Content
    
    var body: some View {
        VStack {
            ForEach(0..<rows, id: \.self) { row in
                HStack {
                    ForEach(0..<columns, id: \.self) { column in
                        content(row, column)
                    }
                }
            }
        }
    }
}


struct ProminentTitle: ViewModifier {
    var text: String
    
    func body(content: Content) -> some View {
        ZStack(alignment: .top) {
            content
            Text(text)
                .font(.largeTitle)
                .foregroundColor(.blue)
        }
    }
}

extension View {
    func prominentTitle(_ text: String) -> some View {
        modifier(ProminentTitle(text: text))
    }
}
