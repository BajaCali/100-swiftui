//
//  ViewsAndModifiersApp.swift
//  ViewsAndModifiers
//
//  Created by Michal on 13.12.2021.
//

import SwiftUI

@main
struct ViewsAndModifiersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
