//
//  AccessibilitySandboxApp.swift
//  AccessibilitySandbox
//
//  Created by Michal on 19.04.22.
//

import SwiftUI

@main
struct AccessibilitySandboxApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
