//
//  Order.swift
//  CupcakeCorner
//
//  Created by Michal on 24.01.2022.
//

import SwiftUI

class Order: ObservableObject {
    static let types = ["Vanilla", "Strawberry", "Chocolate", "Rainbow"]
    
    @Published var data = OrderData()
    
    var type: Int {
        get { data.type }
        set { data.type = newValue }
    }
    var quantity: Int {
        get { data.quantity }
        set { data.quantity = newValue }
    }
    
    @Published var specialRequestEnabled = false {
        didSet {
            if specialRequestEnabled == false {
                addSprinkles = false
                extraFrosting = false
            }
        }
    }
    var addSprinkles: Bool {
        get { data.addSprinkles }
        set { data.addSprinkles = newValue }
    }
    var extraFrosting : Bool {
        get { data.extraFrosting }
        set { data.extraFrosting = newValue }
    }
    
    var address: Address {
        get { data.address }
        set { data.address = newValue }
    }
    
    
    var cost: Double {
        var cost = Double(quantity) * 2.0
        
        cost += (Double(type) / 2)
        
        if extraFrosting {
            cost += Double(quantity)
        }
        
        if addSprinkles {
            cost += Double(quantity) / 2
        }
        
        return cost
    }
    
    func palceOrder() async throws -> OrderData {
        guard let encoded = try? JSONEncoder().encode(data) else {
            print("Failed to encode order")
            throw "Unable to encode Order"
        }
        
        let url = URL(string: "https://reqres.in/api/cupcakes")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let (data, _) = try await URLSession.shared.upload(for: request, from: encoded)
        let decodedOrder = try JSONDecoder().decode(OrderData.self, from: data)
        print(decodedOrder)
        
        return decodedOrder
    }
}


extension String: Error {
    
}
