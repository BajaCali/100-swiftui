//
//  Address.swift
//  CupcakeCorner
//
//  Created by Michal on 25.01.2022.
//

import Foundation

struct Address: Codable {
    var name: String = ""
    var streetAddress: String = ""
    var city: String = ""
    var zip: String = ""
    
    
    var isValid: Bool {
        name.isValid && streetAddress.isValid && city.isValid && zip.isValid
    }
}

extension String {
    var isValid: Bool {
        !self.isEmpty && !self.allSatisfy({ $0 == " " })
    }
}
