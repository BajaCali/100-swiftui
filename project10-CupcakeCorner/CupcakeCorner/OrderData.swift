//
//  OrderData.swift
//  CupcakeCorner
//
//  Created by Michal on 25.01.2022.
//

import Foundation

struct OrderData: Codable {
    static let types = ["Vanilla", "Strawberry", "Chocolate", "Rainbow"]
    
    var type = 0
    var quantity = 3
    
    var extraFrosting = false
    var addSprinkles = false
    
    var address = Address()
}
