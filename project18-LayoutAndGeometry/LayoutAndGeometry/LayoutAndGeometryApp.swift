//
//  LayoutAndGeometryApp.swift
//  LayoutAndGeometry
//
//  Created by Michal on 14.05.22.
//

import SwiftUI

@main
struct LayoutAndGeometryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
