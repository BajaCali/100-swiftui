//
//  ContentView.swift
//  iExepense
//
//  Created by Michal on 27.12.2021.
//

import SwiftUI


struct ContentView: View {
    @StateObject var expenses = Expenses()
    
    @State private var showingAddExpanse = false
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    ForEach(expenses.items.filter({ $0.type == "Personal" })) { item in
                        Cell(item)
                    }
                    .onDelete { indexSet in
                        removeItems(at: indexSet, ofType: "Personal")
                    }
                } header: {
                    Text("Personal")
                }
                
                Section {
                    ForEach(expenses.items.filter({ $0.type == "Business" })) { item in
                        Cell(item)
                    }
                    .onDelete { indexSet in
                        removeItems(at: indexSet, ofType: "Business")
                    }
                } header: {
                    Text("Business")
                }
            }
            .navigationTitle("iExpenses")
            .toolbar {
                Button {
                    showingAddExpanse = true
                } label: {
                    Image(systemName: "plus.circle")
                }
            }
            .sheet(isPresented: $showingAddExpanse) {
                AddView(expenses: expenses)
            }
        }
    }
    
    func removeItems(at offsets: IndexSet, ofType type: String) {
        guard var indexInType = offsets.first else { return }
        
        var index = 0
        while expenses.items[index].type != type { index += 1 }
        
        while indexInType > 0 {
            if expenses.items[index].type == type {
                indexInType -= 1
            }
            index += 1
        }
        
        expenses.items.remove(at: index)
    }
}

struct Cell: View {
    let item: ExpenseItem
    
    init(_ item: ExpenseItem) {
        self.item = item
    }
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(item.name)
                    .font(.headline)
                Text(item.type)
            }
            
            Spacer()
            
            Text(item.amount, format: .currency(code: Locale.current.currencyCode ?? "USD"))
                .foregroundColor(
                    (item.amount <= 10) ?
                        .gray :
                        ((item.amount <= 100) ?
                            .primary :
                                .red)
                )
        }
        .accessibilityElement()
        .accessibilityLabel("\(item.name): \(item.amount, format: .currency(code: Locale.current.currencyCode ?? "USD"))")
        .accessibilityHint(item.type)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
