//
//  Expenses.swift
//  iExpense
//
//  Created by Michal on 28.12.2021.
//

import Foundation

class Expenses: ObservableObject {
    @Published var items = [ExpenseItem]() {
        didSet {
            guard let encoded = try? JSONEncoder().encode(items) else { return }
            
            UserDefaults.standard.set(encoded, forKey: "Items")
        }
    }
    
    init() {
        guard let data = UserDefaults.standard.data(forKey: "Items"),
              let deocdedItems = try? JSONDecoder().decode([ExpenseItem].self, from: data) else {
                  items = []
                  return
              }
        
        items = deocdedItems
    }
}
