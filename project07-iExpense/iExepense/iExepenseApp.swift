//
//  iExepenseApp.swift
//  iExepense
//
//  Created by Michal on 27.12.2021.
//

import SwiftUI

@main
struct iExepenseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
