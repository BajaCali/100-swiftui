//
//  ContentView.swift
//  LenConv
//
//  Created by Michal on 09.12.2021.
//

import SwiftUI

enum Unit: String, CaseIterable { case km, m, ft, yd, mi, nmi}

extension Unit {
    var inKms: Double {
        switch(self) {
        case .km: return 1
        case .m: return 0.001
        case .ft: return 0.000_304_8
        case .yd: return 0.000_914_4
        case .mi: return 1.609_344
        case .nmi: return 1.852
        }
    }
}

struct ContentView: View {
    @State private var inputValue: Double = 10
    private var kms: Double { return inputValue * currentInputUnit.inKms }
    @State private var currentInputUnit: Unit = .km
    
    @FocusState private var inputIsFocused: Bool
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("number", value: $inputValue, format: .number)
                        .keyboardType(.decimalPad)
                        .focused($inputIsFocused)
                    Picker("Choose output unit", selection: $currentInputUnit) {
                        ForEach(Unit.allCases, id: \.self) { unit in
                            Text(unit.rawValue)
                        }
                    }
                    .pickerStyle(.segmented)
                    
                } header: {
                    Text("Convert value in unit")
                }
            
                Section {
                    ForEach(Unit.allCases, id: \.self) { unit in
                        HStack {
                            Spacer()
                            Text("\((kms / unit.inKms).formattedWithSeparator)")
                            Text("\(unit.rawValue)")
                                .frame(width: 30, alignment: .leading)
                        }
                    }
                } header: {
                    Text("Converted value in available units")
                }
                
            }
            .navigationTitle("LenConv")
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Spacer()
                    Button("Done") {
                        inputIsFocused = false
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

// inspiration: https://stackoverflow.com/a/29999137
extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        return formatter
    }()
}

extension Numeric {
    var formattedWithSeparator: String { Formatter.withSeparator.string(for: self) ?? "" }
}
