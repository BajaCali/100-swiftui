//
//  LenConvApp.swift
//  LenConv
//
//  Created by Michal on 09.12.2021.
//

import SwiftUI

@main
struct LenConvApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
