//
//  RatingView.swift
//  Bookworm
//
//  Created by Michal on 26.01.2022.
//

import SwiftUI

struct RatingView: View {
    @Binding var rating: Int
    
    
    var label = ""
    var maximumRaing = 5
    
    var offImage: Image?
    var onImage = Image(systemName: "star.fill")
    
    var offColor = Color.gray
    var onColor = Color.yellow
    
    var body: some View {
        HStack {
            if !label.isEmpty {
                Text(label)
            }
            
            ForEach(1..<maximumRaing + 1, id: \.self) { number in
                image(for: number)
                    .foregroundColor(number > rating ? offColor : onColor)
                    .onTapGesture {
                        rating = number
                    }
            }
        }
        .accessibilityElement()
        .accessibilityLabel("Rating")
        .accessibilityValue(rating == 1 ? "1 star" : "\(rating) stars")
        .accessibilityAdjustableAction { direction in
            switch direction {
            case .increment:
                if rating > 1 { rating -= 1}
            case .decrement:
                if rating < 5 { rating += 1 }
            default: break
            }
            
        }
    }
    
    func image(for number: Int) -> Image {
        switch (number) {
        case ...rating: return onImage
        default: return offImage ?? onImage
        }
    }
}

struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        RatingView(rating: .constant(3))
    }
}
