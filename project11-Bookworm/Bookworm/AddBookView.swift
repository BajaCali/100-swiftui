//
//  AddBookView.swift
//  Bookworm
//
//  Created by Michal on 26.01.2022.
//

import SwiftUI

struct AddBookView: View {
    @Environment(\.managedObjectContext) var moc
    @Environment(\.dismiss) var dismiss
    
    @State private var title = ""
    @State private var author = ""
    @State private var rating = 3
    @State private var genre = "Fantasy"
    @State private var review = ""
    @State private var datePublished = Date.now
    
    @State private var showingAlert = false
    @State private var alertMessage = ""
    
    let genres = ["Fantasy", "Horror", "Kids", "Mistery", "Poetry", "Romance", "Thriller"]
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Name of book", text: $title)
                    TextField("Author's name", text: $author)
                    Picker("Genre", selection: $genre) {
                        ForEach(genres, id: \.self) { genre in
                            Text(genre)
                        }
                    }
                    DatePicker(
                        "Date Published",
                        selection: $datePublished,
                        displayedComponents: [.date]
                    )
                }
                
                Section {
                    TextEditor(text: $review)
                    
                    HStack {
                        Spacer()
                        RatingView(rating: $rating)
                        Spacer()
                    }
                    
                } header: {
                    Text("Review")
                }
                
                Section {
                    Button("Save") {
                        guard self.isFilled() else { return }
                        
                        let newBook = Book(context: moc)
                        newBook.id = UUID()
                        newBook.title = title
                        newBook.author = author
                        newBook.rating = Int16(rating)
                        newBook.genre = genre
                        newBook.review = review
                        newBook.datePublished = datePublished
                        
                        try? moc.save()
                        
                        dismiss()
                    }
                }
            }
            .navigationTitle("Add Book")
            .alert("Please fill necessary parameters", isPresented: $showingAlert) {
                Button("OK", role: .cancel) { }
                Button("Discart", role: .destructive) {
                    dismiss()
                }
            } message: {
                Text(alertMessage)
            }
        }
    }
    
    func isFilled() -> Bool {
        switch(title.isEmpty, author.isEmpty, genre.isEmpty) {
        case (true, _, _): alertMessage = "Please fill in title of the book"
        case (_, true, _): alertMessage = "Please fill in author of the book"
        case (_, _, true): alertMessage = "Please fill in genre of the book"
        default: return true
        }
       
        showingAlert = true
        return false
    }
    
}

struct AddBookView_Previews: PreviewProvider {
    static var previews: some View {
        AddBookView()
    }
}
