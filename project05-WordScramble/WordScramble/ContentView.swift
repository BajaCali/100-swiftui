//
//  ContentView.swift
//  WordScramble
//
//  Created by Michal on 23.12.2021.
//

import SwiftUI

struct ContentView: View {
    @State private var usedWords = [String]()
    @State private var rootWord = ""
    @State private var newWord = ""
    
    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false
    
    @State private var score: Double = 0
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    TextField("Enter your word", text: $newWord)
                        .autocapitalization(.none)
                }
                
                Section {
                    ForEach(usedWords, id: \.self) { word in
                        HStack {
                            Image(systemName: "\(word.count).circle")
                            Text(word)
                        }
                        .accessibilityElement()
//                        .accessibilityLabel("\(word), \(word.count)")
                        .accessibilityLabel(word)
                        .accessibilityHint("\(word.count) letters")
                    }
                }
            }
            .navigationTitle(rootWord.uppercased())
            .onSubmit(addNewWord)
            .onAppear(perform: startGame)
            .alert(errorTitle, isPresented: $showingError) {
                Button("OK", role: .cancel) { }
            } message: {
                Text(errorMessage)
            }
            .toolbar {
                    
                ToolbarItem(placement: .navigationBarLeading) {
                    Text("score: \(score.formatted())")
                }
            
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: startGame) {
                        Image(systemName: "repeat")
                    }
                }
            }
        }
    }
    
    func addNewWord() {
        let word = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        
        newWord = ""

        guard word != rootWord else {
            wordError(title: "Similar to root word", message: "You can't just retype the same word as given.")
            return
        }

        guard word.count >= 3 else {
            wordError(title: "Word too short", message: "You must type longer word!")
            return
        }

        guard isOriginal(word: word) else {
            wordError(title: "Word used already", message: "Be more original")
            return
        }
        
        guard isPossible(word: word) else {
            wordError(title: "Word not possible", message: "You can't spell that word from \(rootWord)")
            return
        }
        
        guard isReal(word: word) else {
            wordError(title: "Word not recognized", message: "You can't just make them up, you know!")
            return
        }
        
        score *= 1 + (Double(usedWords.count) / 10.0)
        score += Double(word.count)
        
        withAnimation {
            usedWords.insert(word, at: 0)
        }
    }
    
    func startGame() {
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                let allWords = startWords.components(separatedBy: "\n")
                rootWord = allWords.randomElement() ?? "silkworm"
                return
            }
        }
        
        fatalError("Could not load start.txt from bundle.")
    }
    
    func isOriginal(word: String) -> Bool {
        !usedWords.contains(word)
    }
    
    func isPossible(word: String) -> Bool {
        var tempWord = rootWord
        
        for letter in word {
            guard let letterIndex = tempWord.firstIndex(of: letter) else {
                return false
            }
            tempWord.remove(at: letterIndex)
        }
        
        return true
    }
    
    func isReal(word: String) -> Bool {
        let nsword = word.utf16
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: nsword.count)
        let misspalledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        
        return misspalledRange.location == NSNotFound
    }
    
    func wordError(title: String, message: String) {
        errorTitle = title
        errorMessage = message
        showingError = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
