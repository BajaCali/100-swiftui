//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Michal on 23.12.2021.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
