//
//  ContentView.swift
//  HotProspects
//
//  Created by Michal on 03.05.22.
//

import SwiftUI

enum ProspectTab: Int, CaseIterable {
    case everyone, contacted, uncontacted
    
    var name: String {
        switch self {
        case .everyone: return "Everyone"
        case .contacted: return "Contacted"
        case .uncontacted: return "Uncontacted"
        }
    }
    
    var symbol: String {
        switch self {
        case .everyone: return "person.3"
        case .contacted: return "checkmark.circle"
        case .uncontacted: return "questionmark.diamond"
        }
    }
    
    var filter: ProspectsView.FilterType {
        switch self {
        case .everyone: return .none
        case .contacted: return .contacted
        case .uncontacted: return .uncontacted
        }
    }
}
    
struct ContentView: View {
    @StateObject var prospects: Prospects
    
    @State private var sorters = [ProspectTab: ProspectsView.SortingType]()
    
    init() {
        _prospects = StateObject(wrappedValue: Prospects())
    }
    
    var body: some View {
        TabView {
            
            ForEach(ProspectTab.allCases, id: \.rawValue) { tab in
                ProspectsView(prospectTab: tab, sorting: sortingBinding(for: tab))
                    .tabItem {
                        Label(tab.name, systemImage: tab.symbol)
                    }
            }
            
            MeView()
                .tabItem {
                    Label("Me", systemImage: "person.crop.square")
                }
        }
        .environmentObject(prospects)
    }
    
    private func sortingBinding(for tab: ProspectTab) -> Binding<ProspectsView.SortingType> {
        Binding(
            get: {
                return self.sorters[tab] ?? .alphabetic
            },
            set: {
                self.sorters[tab] = $0
            }
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
