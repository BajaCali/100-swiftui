//
//  HotProspectsApp.swift
//  HotProspects
//
//  Created by Michal on 03.05.22.
//

import SwiftUI

@main
struct HotProspectsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
