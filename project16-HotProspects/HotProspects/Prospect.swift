//
//  Prospect.swift
//  HotProspects
//
//  Created by Michal on 04.05.22.
//

import Foundation
import SwiftUtilities

class Prospect: Identifiable, Codable {
    var id = UUID()
    var name = "Anonymous"
    var email = ""
    var dateAdded = Date.now
    fileprivate(set) var isContacted = false
}

@MainActor class Prospects: ObservableObject {
    @Published private(set) var people: [Prospect]
    private let saveKey = "SavedData"
    
    init() {
        if let savedProspects = try? readJSONFromDocuments(as: [Prospect].self, filename: saveKey) {
            people = savedProspects
            return
        }
        
        people = []
    }
    
    private func save() {
        do {
            try saveAsJSONToDocuments(people, filename: saveKey)
        } catch {
            print("failed to save prospects with error: \(error.localizedDescription)")
        }
    }
    
    func togleContacted(prospect: Prospect) {
        objectWillChange.send()
        prospect.isContacted.toggle()
        save()
    }
    
    func add(prospect: Prospect) {
        people.append(prospect)
        save()
    }
}
