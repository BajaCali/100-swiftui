//
//  ProspectsView.swift
//  HotProspects
//
//  Created by Michal on 04.05.22.
//

import SwiftUI
import CodeScanner
import UserNotifications

struct ProspectsView: View {
    enum FilterType {
        case none, contacted, uncontacted
        
        func closure(prospect: Prospect) -> Bool {
            switch self {
            case .none:
                return true
            case .contacted:
                return prospect.isContacted
            case .uncontacted:
                return prospect.isContacted == false
            }
        }
    }
    
    enum SortingType: Int, CaseIterable { case alphabetic, recent
        var name: String {
            switch self {
            case .alphabetic: return "By name"
            case .recent: return "Most recent"
            }
        }
        func sorter(lhs: Prospect, rhs: Prospect) -> Bool {
            switch self {
            case .alphabetic:
                return lhs.name < rhs.name
            case .recent:
                return lhs.dateAdded > rhs.dateAdded
            }
        }
    }
    
    let prospectTab: ProspectTab
    @Binding var sorting: SortingType
    
    @EnvironmentObject var prospects: Prospects
    
    @State private var isShowingScanner = false
    
    @State var displayScale: CGFloat = 1.0
    @State private var toggle = false
    
    @State private var showingChangeSorting = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(displayProspects) { prospect in
                    HStack {
                        Capsule()
                            .foregroundColor(prospect.isContacted ? .blue : .green)
                            .frame(width: 5)
                        
                        VStack(alignment: .leading) {
                            Text(prospect.name)
                                .font(.headline)
                            
                            Text(prospect.email)
                                .foregroundColor(.secondary)
                        }
                    }
                    .swipeActions {
                        if prospect.isContacted {
                            Button {
                                prospects.togleContacted(prospect: prospect)
                            } label: {
                                Label("Mark Uncontacted", systemImage: "person.crop.circle.badge.xmark")
                            }
                            .tint(.blue)
                        } else {
                            Button {
                                prospects.togleContacted(prospect: prospect)
                            } label: {
                                Label("Mark Contacted", systemImage: "person.crop.circle.fill.badge.checkmark")
                            }
                            .tint(.green)
                            
                            Button {
                                addNotification(for: prospect)
                            } label: {
                                Label("Remind me", systemImage: "bell")
                            }
                            .tint(.orange)
                        }
                    }
                }
            }
            .navigationTitle(prospectTab.name)
            .toolbar {
                HStack {
                    Button {
                        showingChangeSorting = true
                    } label: {
                        Label("Change Sorting", systemImage: "arrow.up.arrow.down.square")
                    }
                    Button {
                        isShowingScanner = true
                    } label: {
                        Label("Scan", systemImage: "qrcode.viewfinder")
                    }
                }
            }
            .sheet(isPresented: $isShowingScanner) {
                CodeScannerView(codeTypes: [.qr], simulatedData: "Paul Hudson\npaul@hackingwithswift.com", completion: handleScan)
            }
            .confirmationDialog("Select sorting for \(prospectTab.name)", isPresented: $showingChangeSorting, titleVisibility: .visible) {
                ForEach(ProspectsView.SortingType.allCases, id: \.rawValue) { sortingType in
                    Button {
                        sorting = sortingType
                    } label: {
                        Text(sortingType.name)
                    }
                }
            }
        }
    }
    
    private var filteredProspects: [Prospect] {
        prospects.people.filter(prospectTab.filter.closure)
    }
    
    private var displayProspects: [Prospect] {
        filteredProspects
            .sorted(by: sorting.sorter)
    }
    
    private func handleScan(result: Result<ScanResult, ScanError>) {
        isShowingScanner = false
        switch result {
        case let .success(scanResult):
            let details = scanResult.string.components(separatedBy: "\n")
            guard details.count == 2 else { return }
            
            let prospect = Prospect()
            prospect.name = details[0]
            prospect.email = details[1]
            
            prospects.add(prospect: prospect)
        case let .failure(error):
            print("failed to scan qr code with error: \(error)")
        }
    }
    
    private func addNotification(for prospect: Prospect) {
        let center = UNUserNotificationCenter.current()
        
        let addRequest = {
            let content = UNMutableNotificationContent()
            content.title = "Contact \(prospect.name)"
            content.subtitle = prospect.email
            content.sound = UNNotificationSound.default
            
            var dateComponents = DateComponents()
            dateComponents.hour = 9
            let trigger = UNCalendarNotificationTrigger.init(dateMatching: dateComponents, repeats: false)
//            // testing
//            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 2, repeats: false)
            
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            center.add(request)
        }
        
        center.getNotificationSettings { settings in
            if settings.authorizationStatus == .authorized {
                addRequest()
            } else {
                center.requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
                    if success {
                        addRequest()
                    } else {
                        print("not permited")
                    }
                }
            }
        }
    }
}

struct ProspectsView_Previews: PreviewProvider {
    static var previews: some View {
        ProspectsView(prospectTab: .everyone, sorting: .constant(.alphabetic))
    }
}
