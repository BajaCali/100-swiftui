//
//  ContentView.swift
//  CoreDataProject
//
//  Created by Michal on 25.02.22.
//

import CoreData
import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: []) var countries: FetchedResults<Country>
    
    var body: some View {
        VStack {
            List {
                ForEach(countries, id: \.self) { country in
                    Section(country.wrappedFullName) {
                        ForEach(country.candyArray, id: \.self) { candy in
                            Text(candy.wrappedName)
                        }
                    }
                }
            }
            
            Button("Add examples") {
                let c1 = Candy(context: moc)
                c1.name = "Mars"
                c1.origin = Country(context: moc)
                c1.origin?.shortName = "UK"
                c1.origin?.fullName = "United Kingdom"
                
                let c2 = Candy(context: moc)
                c2.name = "KitKat"
                c2.origin = Country(context: moc)
                c2.origin?.shortName = "UK"
                c2.origin?.fullName = "United Kingdom"
                
                let c3 = Candy(context: moc)
                c3.name = "Twix"
                c3.origin = Country(context: moc)
                c3.origin?.shortName = "UK"
                c3.origin?.fullName = "United Kingdom"
                
                let c4 = Candy(context: moc)
                c4.name = "Toblerone"
                c4.origin = Country(context: moc)
                c4.origin?.shortName = "CH"
                c4.origin?.fullName = "Switzerland"
                
                try? moc.save()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
