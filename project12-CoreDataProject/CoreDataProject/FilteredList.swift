//
//  FilteredList.swift
//  CoreDataProject
//
//  Created by Michal on 27.02.22.
//

import CoreData
import SwiftUI

struct FilteredList<ItemType: NSManagedObject, Content: View>: View {
    @FetchRequest var fetchRequest: FetchedResults<ItemType>
    let content: (ItemType) -> Content
    
    var body: some View {
        List(fetchRequest, id: \.self) { item in
            self.content(item)
        }
    }
    
    enum Predicate: String {
        case beginsWith = "BEGINSWITH[cd]"
        case endsWith = "ENDSWITH[cd]"
        case contains = "CONTAINS[cd]"
        case like = "LIKE[cd]"
    }

    /// for `predicate` use: BEGINSWITH, CONTAINS and so on.
    init(filterKey: String,
         predicate: Predicate,
         filterValue: String,
         sortDescriptors: Array<NSSortDescriptor> = [],
         @ViewBuilder content: @escaping (ItemType) -> Content) {
        _fetchRequest = FetchRequest<ItemType>(
            sortDescriptors: sortDescriptors,
            predicate: NSPredicate(format: "%K \(predicate.rawValue) %@",
                                   filterKey,
                                   filterValue)
        )
        self.content = content
    }
}
