//
//  Astronaut.swift
//  Moonshot
//
//  Created by Michal Němec on 30.12.2021.
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
