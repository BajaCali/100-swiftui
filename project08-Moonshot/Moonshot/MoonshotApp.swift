//
//  MoonshotApp.swift
//  Moonshot
//
//  Created by Michal on 31.12.2021.
//

import SwiftUI

@main
struct MoonshotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
