//
//  MissionView.swift
//  Moonshot
//
//  Created by Michal on 31.12.2021.
//

import SwiftUI

struct MissionView: View {
    struct CrewMember {
        let role: String
        let astronaut: Astronaut
    }
    
    let mission: Mission
    let crew: [CrewMember]
    
    init(mission: Mission, astronauts: [String: Astronaut]) {
        self.mission = mission
        
        self.crew = mission.crew.map { member in
            if let astronaut = astronauts[member.name] {
                return CrewMember(role: member.role, astronaut: astronaut)
            } else {
                fatalError("Cannot find \(member.name) in astronaut dictionary.")
            }
        }
    }
    
    var body: some View {
        return GeometryReader { geometry in
            ScrollView {
                VStack {
                    Image(decorative: mission.image)
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: geometry.size.width * 0.6)
                        .padding(.vertical)
                    
                    Text("\(mission.formattedLaunchDate)")
                        .padding(.horizontal)
                        .padding(.vertical, 5)
                        .background(.lightBackground)
                        .clipShape(Capsule(style: .continuous))
                        .accessibilityLabel("Launch date")
                        .accessibilityHint(mission.formattedLaunchDate)
                    
                    VStack(alignment: .leading) {
                        MissionView.Divider()
                        
                        Text("Mission Highlights")
                            .font(.title.bold())
                            .padding(.bottom, 5)
                        
                        Text(mission.description)
                        
                        MissionView.Divider()
                        
                        Text("Crew")
                            .font(.title.bold())
                            .padding(.bottom, 5)
                    }
                    .padding(.horizontal)
                    .accessibilityElement()
                    .accessibilityLabel("MissionHIghlights")
                    .accessibilityHint(mission.description)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(crew, id: \.role) { crewMember in
                                NavigationLink {
                                    AstronautView(astronaut: crewMember.astronaut)
                                } label: {
                                    let imageShape = RoundedRectangle(cornerRadius: 10)
                                    HStack {
                                        Image(crewMember.astronaut.id)
                                            .resizable()
                                            .frame(width: 104, height: 72)
                                            .clipShape(imageShape)
                                            .overlay(
                                                imageShape
                                                    .strokeBorder(.white,
                                                                  lineWidth: 1)
                                            )
                                        
                                        VStack(alignment: .leading) {
                                            Text(crewMember.astronaut.name)
                                                .foregroundColor(.white)
                                                .font(.headline)
                                            
                                            Text(crewMember.role)
                                                .foregroundColor(.secondary)
                                        }
                                    }
                                        .padding(.horizontal)
                                        .accessibilityElement()
                                        .accessibilityLabel(crewMember.astronaut.name)
                                        .accessibilityHint(crewMember.role)
                                        .accessibilityAddTraits(.isButton)
                                }
                            }
                        }
                    }
                }
                .padding(.bottom)
            }
            .navigationTitle(mission.displayName)
            .navigationBarTitleDisplayMode(.inline)
            .background(.darkBackground)
        }
    }
    
    struct Divider: View {
        var body: some View {
            Rectangle()
                .frame(height: 2)
                .foregroundColor(.lightBackground)
                .padding(.vertical)
            
        }
    }
}


struct MissionView_Previews: PreviewProvider {
    static let missions: [Mission] = Bundle.main.decode("missions.json")
    
    static let astronauts: [String: Astronaut] = Bundle.main.decode("astronout.json")
    
    static var previews: some View {
        MissionView(mission: missions[0], astronauts: astronauts)
            .preferredColorScheme(.dark)
    }
}
