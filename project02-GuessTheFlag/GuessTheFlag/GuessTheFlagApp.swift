//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Michal on 10.12.2021.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
