//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Michal on 10.12.2021.
//

import SwiftUI

struct ContentView: View {
    // Game variables
    let totalQuestions = 8
    @State private var questionNumber = 0
    
    @State private var score = 0
    
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    @State private var correctAnswer = Int.random(in: 0...2)
    
    // UI variables
    @State private var scoreTitle = ""
    @State private var showingScore = false
    @State private var showingGameRecap = false
    
    // Animation variables
    @State private var tappedNumber: Int? = nil
    
    let dimBlue = Color(red: 0.1, green: 0.2, blue: 0.45)
    let dimRed = Color(red: 0.76, green: 0.15, blue: 0.26)
    
    let labels = [
        "Estonia": "Flag with three horizontal stripes of equal size. Top stripe blue, middle stripe black, bottom stripe white",
        "France": "Flag with three vertical stripes of equal size. Left stripe blue, middle stripe white, right stripe red",
        "Germany": "Flag with three horizontal stripes of equal size. Top stripe black, middle stripe red, bottom stripe gold",
        "Ireland": "Flag with three vertical stripes of equal size. Left stripe green, middle stripe white, right stripe orange",
        "Italy": "Flag with three vertical stripes of equal size. Left stripe green, middle stripe white, right stripe red",
        "Nigeria": "Flag with three vertical stripes of equal size. Left stripe green, middle stripe white, right stripe green",
        "Poland": "Flag with two horizontal stripes of equal size. Top stripe white, bottom stripe red",
        "Russia": "Flag with three horizontal stripes of equal size. Top stripe white, middle stripe blue, bottom stripe red",
        "Spain": "Flag with three horizontal stripes. Top thin stripe red, middle thick stripe gold with a crest on the left, bottom thin stripe red",
        "UK": "Flag with overlapping red and white crosses, both straight and diagonally, on a blue background",
        "US": "Flag with red and white stripes of equal size, with white stars on a blue background in the top-left corner"
    ]
    
    var body: some View {
        ZStack {
            RadialGradient(stops: [
                .init(color: dimBlue, location: 0.3),
                .init(color: dimRed, location: 0.3)
            ], center: .top, startRadius: 200, endRadius: 700)
                .ignoresSafeArea()
            
            VStack {
                Spacer()
                
                Text("Guess the Flag")
                    .font(.largeTitle.bold())
                
                VStack(spacing: 15) {
                    VStack {
                        Text("Tap the flag of")
                            .font(.headline)
                        Text(countries[correctAnswer])
                            .font(.largeTitle)
                    }
                    .foregroundStyle(.secondary)
                    
                    
                    ForEach(0..<3) { number in
                        Button {
                            flagTapped(number)
                        } label: {
                            let flagChosen = tappedNumber != nil
                            let selected = flagChosen && tappedNumber == number
                            
                            FlagImage(imageName: countries[number])
                                .accessibilityLabel(labels[countries[number], default: "unknown flag"])
                                .rotation3DEffect(
                                    Angle(degrees: selected ? 360 : 0),
                                    axis: (x: 1, y: 1, z: 1)
                                )
                                .animation((tappedNumber != nil) ?
                                            .interpolatingSpring(stiffness: 75, damping: 5) :
                                                .none,
                                           value: tappedNumber)
                                .opacity((tappedNumber != nil && tappedNumber != number) ? 0.5 : 1)
                                .rotation3DEffect(Angle(degrees: (flagChosen && !selected) ? 450 : 0), axis: (x: 1, y: 1, z: 0))
                                .animation((tappedNumber != nil) ? .easeOut(duration: 2) : .none, value: tappedNumber)
                                .scaleEffect((!flagChosen) ? 1 : 0)
                                .animation((!flagChosen) ? .default : .default.delay(3), value: tappedNumber)
                        }
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .background(.regularMaterial)
                .clipShape(RoundedRectangle(cornerRadius: 20))
                
                Spacer()
                Spacer()
                
                Text("Score \(score)")
                    .font(.title.bold())
                
                Spacer()
            }
            .foregroundColor(.white)
            .padding()
            // Alert for asking next question
            .alert(scoreTitle, isPresented: $showingScore) {
                Button("Continue", action: askQuestion)
            } message: {
                Text("""
                Question \(questionNumber) out of \(totalQuestions).
                Your score is now \(score).
                """)
            }
            // Alert for reseting the game
            .alert(isPresented: $showingGameRecap) {
                Alert(
                    title: Text("Game over!"),
                    message: Text("You've scored \(score)"),
                    primaryButton: .default(
                        Text("Play Again"),
                        action: resetGame
                    ),
                    secondaryButton: .destructive(
                        Text("Finish Playing"),
                        action: { exit(0) }
                    )
                )
            }
        }
    }

    // MARK: - Game Logic
    
    func flagTapped(_ number: Int) {
        tappedNumber = number
        
        if number == correctAnswer {
            score += 1
            scoreTitle = "Correct!"
        } else {
            scoreTitle = """
            Wrong...
            That's flag of \(countries[number])
            """
            if score > 0 {
                score -= 1
            }
        }
        
        questionNumber += 1
        
        guard questionNumber < totalQuestions else {
            showingGameRecap = true
            return
        }
        
        showingScore = true
    }
    
    func askQuestion() {
        countries = countries.shuffled()
        correctAnswer = Int.random(in: 0...2)
        tappedNumber = nil
    }
    
    func resetGame() {
        score = 0
        questionNumber = 0
        askQuestion()
    }
}

    // MARK: - Help Views

struct FlagImage: View {
    let imageName: String
    
    var body: some View {
        Image(imageName)
            .renderingMode(.original)
            .clipShape(Capsule())
            .shadow(radius: 5)
    }
}

    // MARK: - Previews

struct ContentViewPreviews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .colorScheme(.dark)
        ContentView()
            .previewDevice(PreviewDevice(rawValue: "iPod touch (7th generation)"))
    }
}
