//
//  CreateNewUserSheet.swift
//  FolkRemeberer
//
//  Created by Michal on 27.04.22.
//

import SwiftUI

struct CreateNewUserSheet: View {
    let user: User

    @ObservedObject var folksViewModel: FolksViewModel

    private let locationFetcher = LocationFetcher()

    @State private var userName = ""
    @State private var presentingName = ""

    @Environment(\.dismiss) var dismiss

    @State private var showingLocation = false
    
    @State private var location: Location? = nil

    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                ScrollView {
                    Image(data: user.jpegData)?
                        .resizable()
                        .scaledToFit()

                    TextField("Your mate's name", text: $userName)
                        .padding(5)
                        .overlay(RoundedRectangle(cornerRadius: 5).stroke())
                        .padding()

                    if showingLocation {
                        Text("Location")
                            .padding()

                        LocationView(locationFetcher: locationFetcher)
                            .frame(width: geometry.size.width, height: 200)
                    } else {
                        Button("Save Location") {
                            if let location = locationFetcher.lastKnownLocation {
                                showingLocation = true
                                
                                self.location = Location(latitude: location.latitude, longitude: location.longitude)
                            }
                        }
                    }

                    Spacer(minLength: 20)
                }
            }
            .onAppear {
                locationFetcher.start()
            }
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button("Discard") {
                        folksViewModel.delete(user: user)
                        dismiss()
                    }
                }
                ToolbarItem(placement: .confirmationAction) {
                    Button("Save") {
                        folksViewModel.changeName(to: userName, for: user)
                        folksViewModel.changeLocation(to: location, for: user)
                        dismiss()
                    }
                }
            }
        }
    }
}

//struct CreateNewUserSheet_Previews: PreviewProvider {
//    static var previews: some View {
//        CreateNewUserSheet()
//    }
//}
