//
//  User.swift
//  FolkRemeberer
//
//  Created by Michal on 21.04.22.
//

import Foundation
import MapKit

struct User: Identifiable, Comparable, Codable, Equatable {
    let id: UUID
    var name: String
    let jpegData: Data
    var location: Location?
    
    init(
        name: String,
        jpegData: Data,
        location: Location? = nil
    ) {
        self.id = UUID()
        self.name = name
        self.jpegData = jpegData
        self.location = location
    }
    
    static func < (lhs: User, rhs: User) -> Bool {
        lhs.name < rhs.name
    }
    
    static func ==(lhs: User, rhs: User) -> Bool {
        lhs.id == rhs.id
    }
}

struct Location: Codable {
    var latitude: CLLocationDegrees
    var longitude: CLLocationDegrees
}
