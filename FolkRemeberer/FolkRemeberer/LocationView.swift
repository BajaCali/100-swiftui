//
//  LocationView.swift
//  FolkRemeberer
//
//  Created by Michal on 29.04.22.
//

import SwiftUI
import MapKit

struct LocationView: View {
    let location: CLLocationCoordinate2D?

    @State private var mapRegion = MKCoordinateRegion()
    
    init(locationFetcher: LocationFetcher) {
        self.location = locationFetcher.lastKnownLocation
    }
    
    init(location: Location) {
        self.location = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
    }

    var body: some View {
        Map(coordinateRegion: $mapRegion)
            .onAppear {
                if let location = location {
                    mapRegion.center = location
                    mapRegion.span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                }
            }
    }
}

//struct LocationView_Previews: PreviewProvider {
//    static var previews: some View {
//        LocationView()
//    }
//}
