//
//  UserDetailScreen.swift
//  FolkRemeberer
//
//  Created by Michal on 27.04.22.
//

import SwiftUI

struct UserDetailScreen: View {
    let user: User
    @ObservedObject var folksViewModel: FolksViewModel
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        ScrollView {
            image
                .resizable()
                .scaledToFit()
                .navigationTitle(user.name)
                .padding(.bottom)
            
            if let location = user.location {
                Text("Where did you met him/her")
                    .padding()
                LocationView(location: location)
                    .frame(height: 200)
            }
            
            Button("Delete user", role: .destructive) {
                folksViewModel.delete(user: user)
                dismiss()
            }
            .padding(.vertical)
            
            Spacer(minLength: 20)
        }
        .navigationTitle(user.name)
    }
    
    private var image: Image {
        Image(data: user.jpegData) ?? Image(systemName: "person")
    }
}

//struct UserDetailScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        UserDetailScreen()
//    }
//}
