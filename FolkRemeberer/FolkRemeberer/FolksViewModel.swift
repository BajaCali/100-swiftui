//
//  ContentView-FolksViewModel.swift
//  FolkRemeberer
//
//  Created by Michal on 21.04.22.
//

import Foundation
import UIKit

@MainActor class FolksViewModel: ObservableObject {
    @Published var folks: [User]
    
    private var saveURL = FileManager.documetnsDirectory.appendingPathComponent("FolksSaved")
    
    private var n = 0
    
    init() {
        do {
            let data = try Data(contentsOf: saveURL)
            folks = try JSONDecoder().decode([User].self, from: data)
        } catch {
            folks = [User]()
        }
    }
    
    @discardableResult
    func createNewUser(uiImage: UIImage) -> User? {
        guard let data = uiImage.jpegData(compressionQuality: 0.8) else { return nil }
        let user = User(
            name: "",
            jpegData: data
        )
        folks.append(user)
        
        save()
        
        return user
    }
    
    private func save() {
        do {
            let data = try JSONEncoder().encode(folks)
            try data.write(to: saveURL, options: [.atomic, .completeFileProtection])
        } catch {
            print("Save failed.")
        }
    }
    
    func delete(user: User) {
        folks.removeAll(where: { $0 == user })
        
        save()
    }
    
    func changeName(to newName: String, for user: User) {
        guard let index = folks.firstIndex(of: user) else { return }
        folks[index].name = newName
        save()
    }
    
    func changeLocation(to newLocation: Location?, for user: User) {
        guard let index = folks.firstIndex(of: user) else { return }
        folks[index].location = newLocation
        save()
    }
}
