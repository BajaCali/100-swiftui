//
//  FileManager+DocumentsDirecory.swift
//  FolkRemeberer
//
//  Created by Michal on 26.04.22.
//

import Foundation

extension FileManager {
    static var documetnsDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}
