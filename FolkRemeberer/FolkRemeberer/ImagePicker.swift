//
//  ImagePicker.swift
//  FolkRemeberer
//
//  Created by Michal on 25.04.22.
//

import PhotosUI
import SwiftUI

struct ImagePicker: View {
    @Binding var image: UIImage?
    var sourceType: SourceType
    
    
    enum SourceType: Int, Identifiable {
        case library, camera
        
        var id: Int {
            self.rawValue
        }
    }
    
    var body: some View {
        switch sourceType {
        case .library:
            PhotoLibraryPicker(image: $image)
        case .camera:
            CameraView(image: $image)
        }
    }
}

struct PhotoLibraryPicker: UIViewControllerRepresentable {
    @Binding var image: UIImage?
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate { // inherits from NSObject & confroms to PHPickerViewControllerDelegate
        var parent: PhotoLibraryPicker

        init(_ parent: PhotoLibraryPicker) {
            self.parent = parent
        }

        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            picker.dismiss(animated: true)

            guard let provider = results.first?.itemProvider else { return }

            if provider.canLoadObject(ofClass: UIImage.self) {
                provider.loadObject(ofClass: UIImage.self) { image, _ in
                    self.parent.image = image as? UIImage
                }
            }
        }
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        var config = PHPickerConfiguration()
        config.filter = .images
        
        let picker = PHPickerViewController(configuration: config)
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        // do nothing
    }
    
    func makeCoordinator() -> Coordinator { // UIKit calls this
        Coordinator(self)
    }
}

struct CameraView: UIViewControllerRepresentable {

    @Binding var image: UIImage?
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var parent: CameraView

        init(_ parent: CameraView) {
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            picker.dismiss(animated: true)
            
            guard let selectedImage = info[.originalImage] as? UIImage else { return }
            self.parent.image = selectedImage
            }
    }
        
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = context.coordinator
        return imagePicker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {

    }
    
    func makeCoordinator() -> Coordinator { // UIKit calls this
        Coordinator(self)
    }
}
