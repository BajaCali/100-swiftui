//
//  FolkRemebererApp.swift
//  FolkRemeberer
//
//  Created by Michal on 21.04.22.
//

import SwiftUI

@main
struct FolkRemebererApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
