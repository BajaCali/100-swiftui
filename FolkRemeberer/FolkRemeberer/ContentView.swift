//
//  ContentView.swift
//  FolkRemeberer
//
//  Created by Michal on 21.04.22.
//

import SwiftUI

struct ContentView: View {
    @State private var inputImage: UIImage?
    
    @StateObject private var vm: FolksViewModel
    
    @State private var showingImagePickerSourceTypeSelector = false
    @State private var showingImagePickerSourceType: ImagePicker.SourceType?
    
    @State private var newUser: User?
    @State private var showingUserDetailSheet = false
    
    init() {
        _vm = StateObject(wrappedValue: FolksViewModel())
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(vm.folks.sorted()) { user in
                    NavigationLink(destination: { UserDetailScreen(user: user, folksViewModel: vm) }) {
                        row(for: user)
                    }
                }
            }.toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                       showingImagePickerSourceTypeSelector = true
                    } label: {
                        Image(systemName: "person.badge.plus")
                    }
                }
            }
            .confirmationDialog("", isPresented: $showingImagePickerSourceTypeSelector) {
                Button("Photo Library") { showingImagePickerSourceType = .library }
                Button("Camera") { showingImagePickerSourceType = .camera }
            }
            .sheet(item: $showingImagePickerSourceType) { sourceType in
                ImagePicker(image: $inputImage, sourceType: sourceType)
            }
            .sheet(item: $newUser) { user in
                CreateNewUserSheet(user: user, folksViewModel: vm)
            }
            .onChange(of: inputImage) { image in
                print("image is here or not")
                guard let image = image else {
                    return
                }
                newUser = vm.createNewUser(uiImage: image)
            }
            .navigationTitle("Rememberer")
        }
    }
    
    private func row(for user: User) -> some View {
        HStack {
            user.image
                .resizable()
                .scaledToFit()
                .frame(width: 80)
            Text(user.name)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension Image {
    init?(data: Data) {
        if let uiImage = UIImage(data: data) {
            self.init(uiImage: uiImage)
            return
        }
        return nil
    }
}

extension User {
    var image: Image {
        Image(data: self.jpegData) ?? Image(systemName: "person")
    }
}
