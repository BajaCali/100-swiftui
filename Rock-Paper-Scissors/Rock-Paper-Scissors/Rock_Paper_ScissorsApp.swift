//
//  Rock_Paper_ScissorsApp.swift
//  Rock-Paper-Scissors
//
//  Created by Michal on 15.12.2021.
//

import SwiftUI

@main
struct Rock_Paper_ScissorsApp: App {
    var body: some Scene {
        WindowGroup {
            RPSView()
        }
    }
}
