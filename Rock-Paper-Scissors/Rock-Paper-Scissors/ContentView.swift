//
//  ContentView.swift
//  Rock-Paper-Scissors
//
//  Created by Michal on 15.12.2021.
//

import SwiftUI

enum Move: String, CaseIterable  { case rock, paper, scissors }
let turns = 8
let accentColor = Color.init(hex: "37BFFF")

struct RPSView: View {
    @State private var turnNumber = 0
    @State private var score = 0
    @State private var toWin: Bool
    @State private var AImove: Move
    @State private var userMove: Move
    @State private var showignFinalMessage = false
    
    init() {
        toWin = .random()
        AImove = Move.allCases.randomElement()!
        userMove = Move.allCases.first!
    }
    
    var body: some View {
        VStack(spacing: 30) {
            Spacer()
            
            VStack {
                Text("The AI have chosen")
                    .font(.system(size: 32))
                Text("\(AImove.rawValue.capitalized)")
                    .font(.system(size: 64))
                    .padding(.bottom)
                Text("What do you need to show in order to")
                Text(toWin.toWinString())
                    .bold()
                    .font(.system(size: 32))
                    .padding(.bottom)
            }
            
            Spacer()
            
            HStack {
                Spacer()
                ForEach(Move.allCases, id: \.self) {move in
                    Button {
                        userTurn(move)
                    } label: {
                        Text("\(move.rawValue)")
                            .font(.system(size: 24))
                    }
                    .padding(10)
                    .background(Color.init(hex: "37BFFF"))
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    
                    Spacer()
                }
                
            }
            
            Spacer()
            
            
        }
        .alert("You scored \(score)!", isPresented: $showignFinalMessage) {
            Button("Play Again") {
                score = 0
                turnNumber = 0
                nextTurn()
            }
        }
    }
    
    func nextTurn() {
        toWin = .random()
        
        var nextMove = Move.allCases.randomElement()!
        while nextMove == AImove { nextMove = Move.allCases.randomElement()! }
        AImove = nextMove
    }
    
    func userTurn(_ userMove: Move) {
        let AI_index: Int = Move.allCases.firstIndex(of: AImove)!
        let correctMoveIndex = (3 + AI_index - ((toWin) ? -1 : +1)) % Move.allCases.count
        if (userMove == Move.allCases[correctMoveIndex]) {
            score += 1
        } else {
            score -= 1
        }
        
        turnNumber += 1
        
        if turnNumber == turns {
            showignFinalMessage = true
        }
        
        nextTurn()
    }
}

extension Bool {
    func toWinString() -> String {
        return self ? "win" : "loose"
    }
}

extension Color { // https://stackoverflow.com/a/56874327
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }

        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RPSView()
    }
}
