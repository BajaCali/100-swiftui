//
//  Favourites.swift
//  SnowSeeker
//
//  Created by Michal on 25.05.22.
//

import Foundation

class Favorites: ObservableObject {
    private var resorts: Set<String>
    private let saveKey = "Favorites"
    
    init() {
        if let storedResorts = UserDefaults.standard.stringArray(forKey: saveKey) {
            resorts = Set(storedResorts)
        } else {
            resorts = []
        }
    }
    
    func contains(_ resort: Resort) -> Bool {
        resorts.contains(resort.id)
    }
    
    func add(_ resort: Resort) {
        objectWillChange.send()
        resorts.insert(resort.id)
        save()
    }
    
    func remove(_ resort: Resort) {
        objectWillChange.send()
        resorts.remove(resort.id)
        save()
    }
    
    func save() {
        UserDefaults.standard.set(Array(resorts), forKey: saveKey)
    }
}

extension Favorites: Equatable {
    static func == (lhs: Favorites, rhs: Favorites) -> Bool {
        lhs.saveKey == rhs.saveKey
    }
}
