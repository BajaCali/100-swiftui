//
//  SnowSeekerApp.swift
//  SnowSeeker
//
//  Created by Michal on 23.05.22.
//

import SwiftUI

@main
struct SnowSeekerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
