//
//  ContentView.swift
//  SnowSeeker
//
//  Created by Michal on 23.05.22.
//

import SwiftUI

enum ResortSort: Equatable {
    case alphabetically
    case favoritesFirst(favorites: Favorites)
    case byCountry
}

extension ResortSort {
    func sort(lhs: Resort, rhs: Resort) -> Bool {
        switch self {
        case .alphabetically:
            return lhs.name < rhs.name
        case let .favoritesFirst(favorites):
            if favorites.contains(lhs) {
                return true
            } else if favorites.contains(rhs) {
                return false
            } else {
                return ResortSort.alphabetically.sort(lhs: lhs, rhs: rhs)
            }
        case .byCountry:
            return lhs.country < rhs.country
        }
    }
}

struct ContentView: View {
    let allResorts: [Resort] = Resort.allResorts // Bundle.main.decode("resorts.json")
    
    @StateObject var favorites = Favorites()
    @State private var searchText = ""
    
    @State private var sorting = ResortSort.alphabetically
    
    var body: some View {
        NavigationView {
            List(resorts) { resort in
                NavigationLink {
                    ResortView(resort: resort)
                } label: {
                    HStack {
                        Image(resort.country)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 40, height: 25)
                            .clipShape(RoundedRectangle(cornerRadius: 5))
                            .overlay {
                                RoundedRectangle(cornerRadius: 5)
                                    .stroke(.black, lineWidth: 1)
                            }
                        VStack(alignment: .leading) {
                            Text(resort.name)
                                .font(.headline)
                            Text("\(resort.runs) runs")
                                .foregroundColor(.secondary)
                        }
                        
                        if favorites.contains(resort) {
                            Spacer()
                            Image(systemName: "heart.fill")
                                .accessibilityLabel("This is a favorite resort")
                                .foregroundColor(.red)
                        }
                    }
                }
                .toolbar {
                    menu
                }
            }
            .navigationTitle("Resorts")
            .searchable(text: $searchText, prompt: "Search for a resort")
            
            // displays first if no resort selected only on .regularSize on width
            // does never display for .compactSize
            WelcomeView()
        }
        .environmentObject(favorites)
    }
    
    var resorts: [Resort] {
        filteredResorts.sorted(by: sorting.sort)
    }
    
    var filteredResorts: [Resort] {
        if searchText.isEmpty {
            return allResorts
        } else {
            return allResorts.filter { $0.name.localizedStandardContains(searchText) }
        }
    }
    
    var menu: some View {
        let options: [(label: String, sorting: ResortSort, icon: String)] = [
            ("Favorites first", .favoritesFirst(favorites: favorites), "heart"),
            ("Alphabetically", .alphabetically, "a"),
            ("By Country", .byCountry, "flag")
        ]
        
        return Menu {
            ForEach(options, id: \.label) { settings in
                Label {
                    Button(settings.label) {
                        sorting = settings.sorting
                    }
                } icon: {
//                    if settings.sorting == sorting {
//                    if sorting ~= settings.sorting {
                    if case settings.sorting = sorting {
                        Image(systemName: settings.icon)
                    }
                }
            }
        } label: {
            Label("Sort by", systemImage: "arrow.up.arrow.down.square")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
