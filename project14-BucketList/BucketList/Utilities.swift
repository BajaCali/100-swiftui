//
//  Utilities.swift
//  BucketList
//
//  Created by Michal on 07.04.22.
//

import Foundation

extension FileManager {
    static func saveStringToDisk(_ content: String, filename: String) {
        // this url is not visible to the user
        // there are the "data" of app
        
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filename)
        
        do {
            try content.write(to: url, atomically: true, encoding: .utf8)
            print("saved!")
        } catch {
            print(error.localizedDescription)
        }
    }
}
