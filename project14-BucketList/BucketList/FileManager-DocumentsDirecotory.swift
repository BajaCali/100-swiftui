//
//  FileManager-DocumentsDirecotory.swift
//  BucketList
//
//  Created by Michal on 12.04.22.
//

import Foundation

extension FileManager {
    static var documetnsDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}
