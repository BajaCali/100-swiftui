//
//  BucketListApp.swift
//  BucketList
//
//  Created by Michal on 06.04.22.
//

import SwiftUI

@main
struct BucketListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
