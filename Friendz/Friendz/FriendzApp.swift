//
//  FriendzApp.swift
//  Friendz
//
//  Created by Michal on 22.03.22.
//

import SwiftUI

@main
struct FriendzApp: App {
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
