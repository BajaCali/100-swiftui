//
//  UserDetailView.swift
//  Friendz
//
//  Created by Michal on 22.03.22.
//

import SwiftUI

struct UserDetailView: View {
    let allUsers: FetchedResults<CachedUser>
    let user: CachedUser
    
    @Binding var showingUserDetail: String?
    
    var body: some View {
        List {
            Section("\(user.firstName)'s Tags") {
                tags()
            }
            
            Section("Infromation") {
                row(of: "Company:", with: user.wrappedCompany)
                row(of: "Address:", with: user.wrappedAddress)
                row(of: "Email:", with: user.wrappedEmail)
                row(of: "Date joined:", with: user.registeredDateDisplay)
            }
            
            Section("About \(user.firstName)") {
                Text(user.wrappedAbout)
            }
            
            Section("\(user.firstName)'s friends") {
                ForEach(user.wrappedFriends) { (friend: CachedFriend) in
                    if let friendId = friend.id,
                       let user = allUsers.filter({ $0.wrappedId == friendId }).first
                    {
                        NavigationLink(destination:{
                            UserDetailView(allUsers: allUsers, user: user, showingUserDetail: $showingUserDetail)
                                .navigationBarBackButtonHidden(true)
                        }) {
                            Text(friend.wrappedName)
                        }
                    } else {
                        Text(friend.wrappedName)
                    }
                }
            }
        }
        .navigationTitle(user.wrappedName)
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarLeading) {
                Button {
                    showingUserDetail = nil
                } label: {
                    Text("Friendz")
                        .foregroundColor(.cyan)
                }
            }
        }
        .listStyle(.insetGrouped)
    }
    
    func row(of key: String, with value: String) -> some View {
        HStack {
            Text(key)
            Spacer()
            Text(value)
        }
    }
    
    func tags() -> some View {
        ZStack {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ForEach(user.wrappedTags, id: \.self) { tag in
                        Text(tag)
                            .padding(.horizontal)
                            .padding(.vertical, 5)
                            .background(.secondary)
                            .clipShape(Capsule(style: .continuous))
                    }
                }
                .padding(.horizontal, 5)
            }
            
            HStack {
                Rectangle()
                    .fill(LinearGradient(
                        gradient: .init(colors: [.init(red: 1, green: 1, blue: 1, opacity: 1), .init(red: 1, green: 1, blue: 1, opacity: 0.001)]),
                        startPoint: .leading,
                        endPoint: .trailing
                    ))
                    .frame(width: 5)
                Spacer()
                Rectangle()
                    .fill(LinearGradient(
                        gradient: .init(colors: [.init(red: 1, green: 1, blue: 1, opacity: 1), .init(red: 1, green: 1, blue: 1, opacity: 0.001)]),
                        startPoint: .trailing,
                        endPoint: .leading
                    ))
                    .frame(width: 5)
            }
        }
    }
}

//struct UserDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserDetailView(allUsers: User.examples, user: User.examples[0], showingUserDetail: .constant(false))
//    }
//}
