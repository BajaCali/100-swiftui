//
//  ContentView.swift
//  Friendz
//
//  Created by Michal on 22.03.22.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: []) private var users: FetchedResults<CachedUser>
    @FetchRequest(sortDescriptors: []) private var friends: FetchedResults<CachedFriend>
    
    @State private var showingUserId: String?
    
    var body: some View {
        NavigationView {
                List {
                    ForEach(users) { user in
                        NavigationLink(
                            tag: user.wrappedId,
                            selection: $showingUserId,
                            destination: {
                                UserDetailView(
                                    allUsers: users,
                                    user: user,
                                    showingUserDetail: $showingUserId)
                                    .navigationBarBackButtonHidden(true)
                            },
                            label: { userRow(user) }
                        )
                    }
                }
            .navigationTitle("Friendz")
            .task {
                await updateUsers()
//                print("Total number of users: \(users.count)")
            }
        }
    }
    
    // launches at start time
    func updateUsers() async {
//        print("number of users in cache: \(users.count)")
        
        // fetch new users
        let fetchedUsers = await User.fetchUsers()
        // transfer them to core data classes
        for user in fetchedUsers {
            await MainActor.run {
                // compare if it changes the cache; if so update it; else discard the fetches results
                // ^ that can happen automaticali since there are constrains on both CachedUser's and CachedFriends's ids
                createCachedUser(from: user)
                
                // save moc
                do {
                    try moc.save()
                } catch {
                    print("[\(#function)] failed to save moc")
                }
            }
        }
    }
    
    @discardableResult
    func createCachedUser(from user: User) -> CachedUser {
        let cachedUser = CachedUser(context: moc)
        
        cachedUser.id = user.id
        cachedUser.company = user.company
        cachedUser.name = user.name
        cachedUser.isActive = user.isActive
        cachedUser.tags = user.tags.joined(separator: " ,")
        cachedUser.registered = user.registered
        cachedUser.about = user.about
        cachedUser.address = user.address
        cachedUser.email = user.email
        cachedUser.age = user.age
        
        for friend in user.friends {
            let cachedFriend: CachedFriend
            
            if let cachedFriendFromCache = friends.filter({ $0.wrappedId == friend.id }).first {
                cachedFriend = cachedFriendFromCache
            } else {
                cachedFriend = createCachedFriend(from: friend)
            }
            
            cachedFriend.addToFriendOfUsers(cachedUser)
            cachedUser.addToFriends(cachedFriend)
        }
        
//        print("[creating] \(cachedUser.firstName)'s friends: \(cachedUser.friends?.count ?? -1)")
        return cachedUser
    }
    
    func createCachedFriend(from friend: Friend) -> CachedFriend {
        let cachedFreind = CachedFriend(context: moc)
        
        cachedFreind.id = friend.id
        cachedFreind.name = friend.name
        
        return cachedFreind
    }
    
    func userRow(_ user: CachedUser) -> some View {
        HStack {
            Capsule()
                .frame(width: 2)
                .foregroundColor(user.isActive ? .green : .gray)
            VStack(alignment: .leading) {
                Text(user.wrappedName)
                    .font(.headline)
                Text(user.wrappedCompany)
                    .font(.caption)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
