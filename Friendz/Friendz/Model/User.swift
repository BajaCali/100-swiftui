//
//  User.swift
//  Friendz
//
//  Created by Michal on 22.03.22.
//

import Foundation

struct User: Codable, Identifiable {
    let id: String
    let isActive: Bool
    let name: String
    let age: Int16
    let company: String
    let email: String
    let address: String
    let about: String
    let registered: Date
    let tags: [String]
    let friends: [Friend]
    
    var firstName: String {
        let separatorIndex = name.firstIndex(of: " ") ?? name.endIndex
        return String(name[..<separatorIndex])
    }
    
    var registeredDateDisplay: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        return dateFormatter.string(from: registered)
    }
    
    static var examples: [User] {
        let u1 = User  (id: "50a48fa3-2c0f-4397-ac50-64da464f9954",
               isActive: false,
               name: "Alford Rodriguez",
               age: 21,
               company: "Imkan",
               email: "alfordrodriguez@imkan.com",
               address: "907 Nelson Street, Cotopaxi, South Dakota, 5913",
               about: "Occaecat consequat elit aliquip magna laboris dolore laboris sunt officia adipisicing reprehenderit sunt. Do in proident consectetur labore. Laboris pariatur quis incididunt nostrud labore ad cillum veniam ipsum ullamco. Dolore laborum commodo veniam nisi. Eu ullamco cillum ex nostrud fugiat eu consequat enim cupidatat. Non incididunt fugiat cupidatat reprehenderit nostrud eiusmod eu sit minim do amet qui cupidatat. Elit aliquip nisi ea veniam proident dolore exercitation irure est deserunt.",
                      registered: ISO8601DateFormatter().date(from: "2015-11-10T01:47:18-00:00")!,
               tags: [
                   "cillum",
                   "consequat",
                   "deserunt",
                   "nostrud",
                   "eiusmod",
                   "minim",
                   "tempor"
               ],
               friends: [
                       Friend(id: "eccdf4b8-c9f6-4eeb-8832-28027eb70155",
                              name: "Dale Dyer"
                             ),
                       Friend(id: "0c395a95-57e2-4d53-b4f6-9b9e46a32cf6",
                       name: "Jewel Sexton"
                             )
               ]
        )
        
        let u2 = User(id: "eccdf4b8-c9f6-4eeb-8832-28027eb70155",
                      isActive: true,
                      name: "Gale Dyer",
                      age: 28,
                      company: "Cemention",
                      email: "galedyer@cemention.com",
                      address: "652 Gatling Place, Kieler, Arizona, 1705",
                      about: "Laboris ut dolore ullamco officia mollit reprehenderit qui eiusmod anim cillum qui ipsum esse reprehenderit. Deserunt quis consequat ut ex officia aliqua nostrud fugiat Lorem voluptate sunt consequat. Sint exercitation Lorem irure aliquip duis eiusmod enim. Excepteur non deserunt id eiusmod quis ipsum et consequat proident nulla cupidatat tempor aute. Aliquip amet in ut ad ullamco. Eiusmod anim anim officia magna qui exercitation incididunt eu eiusmod irure officia aute enim.",
                      registered: ISO8601DateFormatter().date(from: "2014-07-05T04:25:04-01:00")!,
                      tags: [
                        "irure",
                        "labore",
                        "et",
                        "sint",
                        "velit",
                        "mollit",
                        "et"
                      ],
                      friends: [
                        Friend(id: "1c18ccf0-2647-497b-b7b4-119f982e6292",
                                    name: "Daisy Bond"),
                        Friend(id: "a1ef63f3-0eab-49a8-a13a-e538f6d1c4f9",
                                    name: "Tanya Roberson")
                      ]
        )
        
        return [u1, u2]
    }
    
    func friendsAsUsers(allUsers: [User]) -> [User] {
        let friendsIds = friends.map { $0.id }
        return allUsers.filter { user in
            friendsIds.contains(user.id)
        }
    }
    
    static func fetchUsers() async -> [User] {
        let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json")!
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let decodedUsers = try decoder.decode([User].self, from: data)
            return decodedUsers
        } catch {
            print("[\(#function)] failed to download/decode")
        }
        
        return User.examples
    }
}
