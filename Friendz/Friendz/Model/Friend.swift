//
//  Friend.swift
//  Friendz
//
//  Created by Michal on 22.03.22.
//

import Foundation

struct Friend: Codable, Identifiable {
    let id: String
    let name: String
}
