//
//  CachedFriend+CoreDataProperties.swift
//  Friendz
//
//  Created by Michal on 24.03.22.
//
//

import Foundation
import CoreData


extension CachedFriend {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedFriend> {
        return NSFetchRequest<CachedFriend>(entityName: "CachedFriend")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var friendOfUsers: NSSet?

}

// MARK: Generated accessors for friendOfUsers
extension CachedFriend {

    @objc(addFriendOfUsersObject:)
    @NSManaged public func addToFriendOfUsers(_ value: CachedUser)

    @objc(removeFriendOfUsersObject:)
    @NSManaged public func removeFromFriendOfUsers(_ value: CachedUser)

    @objc(addFriendOfUsers:)
    @NSManaged public func addToFriendOfUsers(_ values: NSSet)

    @objc(removeFriendOfUsers:)
    @NSManaged public func removeFromFriendOfUsers(_ values: NSSet)

}

extension CachedFriend : Identifiable {

}
