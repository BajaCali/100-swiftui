//
//  CachedFriend+WrappedValues.swift
//  Friendz
//
//  Created by Michal on 24.03.22.
//

import Foundation

extension CachedFriend {
    var wrappedId: String { id ?? "" }
    var wrappedName: String {
        name ?? "Unknown name"
    }
}
