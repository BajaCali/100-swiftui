//
//  CachedUser+WrappedValues.swift
//  Friendz
//
//  Created by Michal on 24.03.22.
//

import Foundation

extension CachedUser {
    var wrappedId: String { id ?? "" }
    var wrappedCompany: String { company ?? "Unknown company" }
    var wrappedName: String { name ?? "Unknown name" }
    var wrappedTags: [String] {
        if let tagString = tags {
            return tagString.split(separator: ",").map { String($0).trimmingCharacters(in: .whitespaces) }
        }
        return []
    }
    var wrappedRegistered: Date { registered! }
    var wrappedAbout: String { about ?? "" }
    var wrappedAddress: String { address ?? "Unknown address" }
    var wrappedEmail: String { email ?? "Unknown email" }
    var wrappedFriends: [CachedFriend] {
        let set = friends as? Set<CachedFriend> ?? []
        
        return set.sorted {
            $0.wrappedName < $1.wrappedName
        }
    }
    
    var firstName: String {
        let separatorIndex = wrappedName.firstIndex(of: " ") ?? wrappedName.endIndex
        return String(wrappedName[..<separatorIndex])
    }
    
    var registeredDateDisplay: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        return dateFormatter.string(from: wrappedRegistered)
    }
}
