# Friendz learning project

Main features and technology included/learned:
- [x] SwiftUI
- [x] CoreData
- [x] Cycle navigation
- [x] Fetching and decoding JSON from the Internet
- [x] Custom tags view & Enchanted row in SwiftUI's List

## Cycle navigation

ListScreen -> Cycle users inside of DetailView; user's friends goes to another DetailView with selected friend

ListScreen <- Tapping back/Friendz button always lead to ListScreen

## Demo

<img src='Docs/friendz_demo.gif' width='250'>
