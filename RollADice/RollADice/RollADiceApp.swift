//
//  RollADiceApp.swift
//  RollADice
//
//  Created by Michal on 17.05.22.
//

import SwiftUI

@main
struct RollADiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
