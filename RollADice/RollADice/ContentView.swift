//
//  ContentView.swift
//  RollADice
//
//  Created by Michal on 17.05.22.
//

import SwiftUI

struct ContentView: View {
    
    @State private var lastRoll: Roll = Roll(sided: .six, numberOfDices: 1)
    @State private var history = [Roll]()
    @State private var currentDiceType = Sided.six
    @State private var numberOfDices = 1.0
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    
                    Picker("Select type of dice", selection: $currentDiceType) {
                        ForEach(Sided.allCases) { sided in
                            Text("\(sided.rawValue)")
                                .tag(sided)
                        }
                    }
                    .pickerStyle(.segmented)
                    .padding()
                    
                    Slider(value: $numberOfDices, in: 1...5, step: 1.0)
                        .padding()
                    
                    
                    Text("\(lastRoll.sum)")
                        .font(.largeTitle)
                        .padding()
                        .frame(width: 100)
                        .transition(.move(edge: .trailing).combined(with: .opacity))
                        .id(lastRoll.id)
                    
                    List(history, id: \.self) { roll in
                        Text("\(roll.sum)")
                    }
                }
                
                VStack {
                    Spacer()
                    
                    HStack {
                        Button(role: .destructive) {
                            reset()
                        } label: {
                            Label("Clear", systemImage: "xmark.square")
                        }
                        .buttonStyle(.bordered)
                        .clipShape(Capsule(style: .continuous))
                        
                        Button {
                            roll()
                        } label: {
                            Label("Roll", systemImage: "dice")
                                .frame(maxWidth: .infinity)
                        }
                        .buttonStyle(.borderedProminent)
                        .clipShape(Capsule(style: .continuous))
                    }
                    .padding()
                }
            }
            .navigationTitle(
                "Roll "
                + "\(numberOfDices == 1 ? "a" : String(Int(numberOfDices))) "
                + "\(currentDiceType.display) "
                + "Dice\(numberOfDices > 1 ? "s" : "")"
            )
        }
    }
    
    func roll() {
        withAnimation{
            history.insert(lastRoll, at: 0)
            lastRoll = lastRoll.nextRoll
        }
    }
    
    func reset() {
        withAnimation {
            history = []
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
