//
//  Roll.swift
//  RollADice
//
//  Created by Michal on 17.05.22.
//

import Foundation

struct Roll: Identifiable {
    let id = UUID()
    let dices: [Dice]
    let timestamp: Date
    
    init(sided: Sided, numberOfDices: Int) {
        assert(numberOfDices > 0)
        self.timestamp = Date.now
        self.dices = Array<Dice>(repeating: Dice(sided), count: numberOfDices)
    }
    
    var sum: Int {
        dices.reduce(0, { $0 + $1.rolledNumber })
    }
}

extension Roll: Equatable {
    static func == (lhs: Roll, rhs: Roll) -> Bool {
        lhs.id == rhs.id
    }
    
    
}

extension Roll: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
}

extension Roll {
    var nextRoll: Roll {
        Roll(sided: self.dices[0].sided, numberOfDices: self.dices.count)
    }
}
