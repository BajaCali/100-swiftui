//
//  Dice.swift
//  RollADice
//
//  Created by Michal on 17.05.22.
//

import Foundation

enum Sided: Int, CaseIterable, Identifiable {
    var id: Int {
        self.rawValue
    }
    
    case four = 4
    case six = 6
    case eight = 8
    case ten = 10
    case twelve = 12
    case twenty = 20
    case hundred = 100
    
    var display: String {
        switch self {
        case .four:
            return "4-sided"
        case .six:
            return "6-sided"
        case .eight:
            return "8-sided"
        case .ten:
            return "10-sided"
        case .twelve:
            return "12-sided"
        case .twenty:
            return "20-sided"
        case .hundred:
            return "100-sided"
        }
    }
}

struct Dice {
    var rolledNumber: Int
    let sided: Sided
    
    init(_ sided: Sided) {
        self.sided = sided
        self.rolledNumber = Int.random(in: 1...sided.rawValue)
    }
    
    mutating func roll() {
        self.rolledNumber = Int.random(in: 1...sided.rawValue)
    }
}
