//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Michal on 16.12.2021.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
