//
//  ContentView.swift
//  BetterRest
//
//  Created by Michal on 16.12.2021.
//

import CoreML
import SwiftUI

struct ContentView: View {
    @State private var wakeUp = defaultWakeTime
    @State private var sleepAmountView = 16
    private var sleepAmountCalculation: Double {
        Double(sleepAmountView) / 4.0 + 4.0
    }
    @State private var coffeeAmount = 1
    
    private var suggestedBedTime : String {
        calculateBedtime() ?? "Error"
    }
    
    @State private var showingAlert = false
    
    static private var defaultWakeTime: Date {
        var components = DateComponents()
        components.hour = 7
        components.minute = 30
        return Calendar.current.date(from: components) ?? Date.now
    }
    
    var body: some View {
        NavigationView {
            Form  {
                FormInput(headline: "What is your daily coffee intake?") {
                    Stepper("I have \(coffeeAmount) coffee\((coffeeAmount == 1) ? "" : "s") daily", value: $coffeeAmount, in: 0...10)
                }
                
                FormInput(headline: "When do you want to wake up?") {
                    DatePicker("I want to wake up at", selection: $wakeUp, displayedComponents: .hourAndMinute)
                }
                
                FormInput(headline: "What is your desired amount of sleep?") {
                    Picker("I want to sleep \(sleepAmountCalculation.formatted())", selection: $sleepAmountView) {
                        ForEach(0..<33) { number in
                            Text("\((4.0 + Double(number) / 4).formatted()) hours")
                        }
                        
                    }
                }
                
                Section {
                    HStack {
                        Spacer()
                        Text(suggestedBedTime)
                            .font(.system(size: 64))
                        Spacer()
                    }
                } header: {
                    Text("Suggested bed time")
                }
                
            }
            .navigationTitle("BetterRest")
            .alert("Calculation error", isPresented: $showingAlert) {
                Button("OK") { }
            } message: {
                Text("An error occured during calculation. Sorry.")
            }
        }
    }
    
    func calculateBedtime() -> String? {
        do {
            let config = MLModelConfiguration()
            let model = try SleepCalculator(configuration: config)
            
            let components = Calendar.current.dateComponents([.minute, .hour], from: wakeUp)
            let hour = components.hour ?? 0
            let minute = components.minute ?? 0
            
            let wakeUpSecond = hour * 3600 + minute * 60
            
            let predeiction = try model.prediction(wake: Double(wakeUpSecond), estimatedSleep: Double(sleepAmountCalculation), coffee: Double(coffeeAmount))
            
            let sleepTime = wakeUp - predeiction.actualSleep
            
            return sleepTime.formatted(date: .omitted, time: .shortened)
            
        } catch {
            showingAlert = true
            return nil
        }
    }
}

struct FormInput<Content: View>: View {
    let headline: String
    @ViewBuilder let content: () -> Content
    
    var body: some View {
        Section {
            content()
        } header: {
            Text(headline)
        }
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

