//
//  ContentView.swift
//  Instafilter
//
//  Created by Michal on 30.03.22.
//

import CoreImage
import CoreImage.CIFilterBuiltins
import SwiftUI

struct ContentView: View {
    @State private var image: Image?
    @State private var filterIntesity = 0.5
    @State private var filterScale = 0.5
    @State private var filterRadius = 0.5
    
    @State private var showingImagePicker = false
    @State private var inputImage: UIImage?
    @State private var processedImage: UIImage?
    
    @State private var currentFiler: CIFilter = CIFilter.sepiaTone()
    let context = CIContext()
    
    @State private var showingFilterSheet = false
    
    var body: some View {
        NavigationView {
            VStack {
                ZStack {
                    Rectangle()
                        .fill(.secondary)
                    
                    Text("Tap to select a picture")
                        .foregroundColor(.white)
                        .font(.headline)
                    
                    image?
                        .resizable()
                        .scaledToFit()
                }
                
                .onTapGesture {
                    showingImagePicker = true
                }
                
                VStack {
                    HStack {
                        Text("Intesity")
                        Slider(value: $filterIntesity)
                            .padding()
                    }
                    
                    HStack {
                        Text("Radius")
                        Slider(value: $filterRadius)
                            .padding()
                    }
                    
                    HStack {
                        Text("Scale")
                        Slider(value: $filterScale)
                            .padding()
                    }
                }
                .onChange(of: filterIntesity, perform: { _ in applyProcessing() })
                
                HStack {
                    Button("Change Filter") {
                        // Change filter
                        showingFilterSheet = true
                    }
                    
                    Spacer()
                    
                    if (image != nil) {
                        Button("Save", action: save)
                    }
                }
            }
            .padding([.horizontal, .bottom])
            .navigationTitle("Instafilter")
            .onChange(of: inputImage, perform: { _ in loadImage() })
            .sheet(isPresented: $showingImagePicker) {
                ImagePicker(image: $inputImage)
            }
            .confirmationDialog("Select a filter", isPresented: $showingFilterSheet) {
                Button("Cristallize") { setFilter(.crystallize())}
                Button("Edges") { setFilter(.edges())}
                Button("Gaussian Blur") { setFilter(.gaussianBlur())}
                Button("Pixellate") { setFilter(.pixellate())}
                Button("Unsharp Mask") { setFilter(.unsharpMask())}
                Button("Vignette") { setFilter(.vignette())}
                Button("Gloom") { setFilter(.gloom())}
                Button("Color Invert") { setFilter(.colorInvert())}
                Button("Tester") { setFilter(.spotLight())}
                Button("Cancel", role: .cancel) { }
            }
        }
    }
    
    func loadImage() {
        guard let inputImage = inputImage else { return }
        let beginImage = CIImage(image: inputImage)
        currentFiler.setValue(beginImage, forKey: kCIInputImageKey)
        applyProcessing()
    }
    
    func save() {
        guard let processedImage = processedImage else { return }
        
        let imageSaver = ImageSaver()
        imageSaver.succesHandler = {
            print("Save finished")
        }
        imageSaver.errorHandler = {
            print("[\(#function)] Oops error happened: \($0.localizedDescription)")
        }
        
        imageSaver.writeToPhotoAlbum(image: processedImage)
    }
    
    func applyProcessing() {
        let saveSet = { (inputKey: String, amount: Double, mulitplier: Double) in
            if (currentFiler.inputKeys.contains(inputKey)) {
                currentFiler.setValue(amount * mulitplier, forKey: inputKey)
            }
        }
        
        saveSet(kCIInputIntensityKey, filterIntesity, 1)
        saveSet(kCIInputRadiusKey, filterRadius, 200)
        saveSet(kCIInputScaleKey, filterScale, 10)
        
        if (currentFiler.inputKeys.contains(kCIInputIntensityKey)) {
            currentFiler.setValue(filterIntesity, forKey: kCIInputIntensityKey)
        }
        
        guard let ouputImage = currentFiler.outputImage else { return }
        
        if let cgimage = context.createCGImage(ouputImage, from: ouputImage.extent) {
            let uiImage = UIImage(cgImage: cgimage)
            image = Image(uiImage: uiImage)
            processedImage = uiImage
        }
    }
    
    func setFilter(_ filter: CIFilter) {
        currentFiler = filter
        loadImage()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
