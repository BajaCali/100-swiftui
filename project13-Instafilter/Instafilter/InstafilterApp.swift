//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by Michal on 30.03.22.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
