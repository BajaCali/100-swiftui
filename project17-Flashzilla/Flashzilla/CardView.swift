//
//  CardView.swift
//  Flashzilla
//
//  Created by Michal on 09.05.22.
//

import SwiftUI

struct CardView: View {
    let card: Card
    var onWrong: (() -> Void)? = nil
    var onSuccess: (() -> Void)? = nil
    
    @State private var isShowingAnswer = false
    @State private var offset = CGSize.zero
    
    @Environment(\.accessibilityDifferentiateWithoutColor) var differentiateWithoutColor
//    @Environment(\.accessibilityVoiceOverEnabled) var voiceOverEnabled
    var voiceOverEnabled = false
    
    @State private var feedback = UINotificationFeedbackGenerator()
    
    var body: some View {
        ZStack {
            Rectangle()
                .fill(
                    differentiateWithoutColor
                    ? .white
                    : .white.opacity(1 - Double(abs(offset.width / 50)))
                )
                .background(
                    differentiateWithoutColor || offset == .zero
                    ? .clear
                    : offset.width > 0 ? .green : .red
                )
                .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
                .shadow(radius: 10)
            
            VStack {
                if voiceOverEnabled {
                    Text(isShowingAnswer ? card.answer : card.prompt)
                } else  {
                    Text(card.prompt)
                        .font(.largeTitle)
                    
                    if isShowingAnswer {
                        Text(card.answer)
                            .font(.title)
                            .foregroundColor(.gray)
                    }
                }
            }
            .padding()
            .multilineTextAlignment(.center)
            .foregroundColor(.black)
        }
        .frame(width: 450, height: 250)
        .rotationEffect(.degrees(Double(offset.width) / 5))
        .offset(x: offset.width * 5, y: 0)
        .opacity(2 - Double(abs(offset.width / 50)))
        .accessibilityAddTraits(.isButton)
        .gesture(
            DragGesture()
                .onChanged { gesture in
                    offset = gesture.translation
                    feedback.prepare()
                }
                // user releases the card
                .onEnded { _ in
                    if abs(offset.width) > 100 {
                        if offset.width > 0 {
                            feedback.notificationOccurred(.success)
                            withAnimation {
                                onSuccess?()
                            }
                        } else {
                            feedback.notificationOccurred(.error)
                            onWrong?()
                            offset = .zero
                        }
                    } else {
                        // return back
                        withAnimation(.spring()) {
                            offset = .zero
                        }
                    }
                    
                }
        )
        .onTapGesture {
            isShowingAnswer.toggle()
        }
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(card: .example)
    }
}
