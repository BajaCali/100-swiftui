//
//  EditCardsView.swift
//  Flashzilla
//
//  Created by Michal on 10.05.22.
//

import SwiftUI

struct EditCardsView: View {
    private let cardsPersistenceInteractor = CardsPersistenceInteractor()
    
    @Environment(\.dismiss) var dismiss
    
    @State var cards = [Card]()
    
    @State private var newPrompt = ""
    @State private var newAnswer = ""
    
    var body: some View {
        NavigationView {
            
            List {
                // MARK: - Adding new card
                Section {
                    TextEditor(text: $newPrompt)
                    TextField("Answer", text: $newAnswer)
                    HStack {
                        Spacer()
                        Button {
                            addNewCard()
                        } label: {
                            Text("Add")
                                .bold()
                        }
                    }
                } header: {
                    Text("Add new question")
                }
                
                // MARK: - List of card
                Section {
                    ForEach(0..<cards.count, id: \.self) { index in
                        let card = cards[index]
                        VStack(alignment: .leading) {
                            Text(card.prompt)
                                .font(.headline)
                            
                            Text(card.answer)
                                .foregroundColor(.secondary)
                        }
                    }
                    .onDelete(perform: removeCard)
                } header: {
                    Text("Cards in the game")
                }
            }
            .navigationTitle("Edit Cards")
            .toolbar {
                Button("Done") {
                    dismiss()
                }
            }
            .listStyle(.grouped)
            .onAppear(perform: loadData)
        }
    }
    
    // MARK: - Handling handling cards
    
    private func addNewCard() {
        let trimmedPrompt = newPrompt.trimmingCharacters(in: .whitespaces)
        let trimmedAnswer = newAnswer.trimmingCharacters(in: .whitespaces)
        guard !trimmedPrompt.isEmpty && !trimmedPrompt.isEmpty else { return }
        
        let card = Card(prompt: trimmedPrompt, answer: trimmedAnswer)
        withAnimation {
            cards.insert(card, at: 0)
        }
        
        newPrompt = ""
        newAnswer = ""
        save()
    }
    
    private func removeCard(at offsets: IndexSet) {
        withAnimation {
            cards.remove(atOffsets: offsets)
        }
        save()
    }
    
    // MARK: - Persistence
    
    private func save() {
        cardsPersistenceInteractor.saveCards(cards)
    }
    
    private func loadData() {
        cards = cardsPersistenceInteractor.loadCards()
    }
}

struct EditCardsView_Previews: PreviewProvider {
    static var previews: some View {
        EditCardsView(cards: [.example])
    }
}
