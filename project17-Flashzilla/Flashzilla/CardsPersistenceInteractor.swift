//
//  CardsPersistenceInteractor.swift
//  Flashzilla
//
//  Created by Michal on 12.05.22.
//

import Foundation
import SwiftUtilities

class CardsPersistenceInteractor {
    private let cardsFilename = "Cards"
    
    func loadCards() -> [Card] {
        if let cards =  try? readJSONFromDocuments(as: [Card].self, filename: cardsFilename) {
            return cards
        }
        return []
    }
    
    func saveCards(_ cards: [Card]) {
        try? saveAsJSONToDocuments(cards, filename: cardsFilename)
    }
}
