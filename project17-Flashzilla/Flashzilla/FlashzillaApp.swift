//
//  FlashzillaApp.swift
//  Flashzilla
//
//  Created by Michal on 08.05.22.
//

import SwiftUI

@main
struct FlashzillaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

struct Constants {
    static let saveKey = "FlashzillaCards"
}
