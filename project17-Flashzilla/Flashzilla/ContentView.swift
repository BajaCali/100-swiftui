//
//  ContentView.swift
//  Flashzilla
//
//  Created by Michal on 08.05.22.
//

import SwiftUI

extension View {
    func stacked(at position: Int, total: Int) -> some View {
        let offset = Double(total - position)
        return self.offset(x: 0, y: offset * 10)
    }
}

struct ContentView: View {
    private let cardsPersistenceInteractor = CardsPersistenceInteractor()
    
    @State private var cards = [Card]()
    
    let timer = Timer.publish(every: 1, on: .current, in: .common).autoconnect()
    @State private var timeReamining = 100
    @State private var isActive = true
    
    @Environment(\.accessibilityDifferentiateWithoutColor) var differentiateWithoutColor
//    @Environment(\.accessibilityVoiceOverEnabled) var voiceOverEnabled
    var voiceOverEnabled = false
    
    @Environment(\.scenePhase) var scenePhase
    
    @State private var showingEdintScreen = false
    
    var body: some View {
        ZStack {
            Image("background")
                .resizable()
            
            VStack {
                // MARK: - Timer
                Text("Time: \(timeReamining)")
                    .font(.largeTitle)
                    .foregroundColor(.white)
                    .padding(.horizontal, 20)
                    .padding(.vertical, 5)
//                    .background(.black.opacity(0.75))
                    .clipShape(Capsule())
                
                // MARK: - CardStack
                ZStack {
                    ForEach(0..<cards.count, id: \.self) { index in
                        CardView(card: cards[index]) {
                            putCardBackToStack(at: index)
                        } onSuccess: {
                            removeCard(at: index)
                        }
                        .stacked(at: index, total: cards.count)
                        .allowsHitTesting(index == cards.count - 1)
                        .accessibilityHidden(index < cards.count - 1)
                    }
                }
                .allowsHitTesting(timeReamining > 0)
                
                if cards.isEmpty {
                    Button("Start Again", action: resetCards)
                        .padding()
//                        .background(.white)
                        .foregroundColor(.black)
                        .clipShape(Capsule())
                }
                
            }
            
            // MARK: - Edit Cards (plus button)
            VStack {
                HStack {
                    Spacer()
                    
                    Button {
                        showingEdintScreen = true
                    } label: {
                        Image(systemName: "plus.circle")
                            .padding()
//                            .background(.black.opacity(0.7))
                            .clipShape(Circle())
                    }
                }
                
                Spacer()
            }
            .foregroundColor(.white)
            .font(.largeTitle)
            .padding()
            
            // MARK: - Correct/Wrong buttons for accessibility
            if differentiateWithoutColor || voiceOverEnabled {
                VStack {
                    Spacer()

                    HStack {
                        Button {
                            withAnimation {
                                removeCard(at: cards.count - 1)
                            }
                        } label: {
                            Image(systemName: "xmark.circle")
                                .padding()
//                                    .background(.black.opacity(0.7))
                                .clipShape(Circle())
                        }
                        .accessibilityLabel("Wrong")
                        .accessibilityHint("Mark your answer as being incorrect")
                        
                        Spacer()
                        
                        Button {
                            withAnimation {
                                removeCard(at: cards.count - 1)
                            }
                        } label: {
                            Image(systemName: "checkmark.circle")
                                .padding()
//                            .background(.black.opacity(0.7))
                                .clipShape(Circle())
                        }
                        .accessibilityLabel("Correct")
                        .accessibilityHint("Mark your answer as being correct")
                    }
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .padding()
                }
            }
       }
        .onReceive(timer) { timer in
            guard isActive else { return }
            if timeReamining > 0 {
                timeReamining -= 1
            }
        }
        .onChange(of: scenePhase) { newPhase in
            if newPhase == .active && !cards.isEmpty {
                isActive = true
            } else if newPhase == .inactive {
                isActive = false
            }
        }
        .sheet(isPresented: $showingEdintScreen, onDismiss: resetCards, content: EditCardsView.init)
        .onAppear(perform: resetCards)
    }
    
    // MARK: - Cards handling
    func removeCard(at index: Int) {
        guard index >= 0 else { return }
        cards.remove(at: index)
        
        if cards.isEmpty {
            isActive = false
        }
    }
    
    func putCardBackToStack(at index: Int) {
        // when no card or the last card do nothing - leave the last card there
        guard index >= 0 && cards.count > 1 else { return }
        var indexOfCardToSwapWith = 0
        while indexOfCardToSwapWith == index {
            indexOfCardToSwapWith = Int.random(in: 0..<cards.count)
        }
        // remove card from front
        cards.swapAt(index, indexOfCardToSwapWith)
        print(cards)
        // find new place (not the first
        
    }
    
    func resetCards() {
        loadData()
        
        cards.shuffle()
        
        timeReamining = 100
        isActive = true
    }
    
    // MARK: - Persistence
    private func loadData() {
        cards = cardsPersistenceInteractor.loadCards()
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
