//
//  Card.swift
//  Flashzilla
//
//  Created by Michal on 09.05.22.
//

import Foundation

struct Card: Codable, Equatable, Identifiable {
    let id: UUID
    let prompt: String
    let answer: String
    
    init(prompt: String, answer: String) {
        self.id = UUID()
        self.prompt = prompt
        self.answer = answer
    }
    
    static let example = Card(prompt: "Who played the 13th Doctor in Doctor Who?", answer: "Jodie Whittaker")
}
