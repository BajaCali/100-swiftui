//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Michal on 06.12.2021.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
