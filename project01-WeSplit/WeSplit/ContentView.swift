//
//  ContentView.swift
//  WeSplit
//
//  Created by Michal on 06.12.2021.
//

import SwiftUI

struct ContentView: View {
    @State private var checkAmount = 0.0
    @State private var numberOfPeople = 2
    @State private var tipPercentage = 20
    @FocusState private var amountIsFocused: Bool
    
    private var totalWithTip: Double {
        let tipSelection = Double(tipPercentage)
        
        return checkAmount * (1 + (tipSelection / 100))
    }
    
    private var totalPerPerson: Double {
        let peopleCount = Double(numberOfPeople + 2)
        
        return totalWithTip / peopleCount
    }
    
    let tipPercentages = [10, 15, 20, 25, 0]
    
    let students = ["Harry", "Hermione", "Ron"]
    @State private var selectedStudent = "Harry"
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", value: $checkAmount, format: .currency(code: Locale.current.currencyCode ?? "CZK"))
                        .keyboardType(.decimalPad)
                        .focused($amountIsFocused)
                    Picker("Number of people", selection: $numberOfPeople) {
                        ForEach(2..<100) {
                            Text("\($0)")
                        }
                    }
                }
                
                Section {
                    Stepper("Tip percentage \(tipPercentage) %", value: $tipPercentage, in: 0...25, step: 5)
                } header: {
                    Text("Tip percentage")
                }
                
                Section {
                    Text(totalPerPerson, format: .currency(code: Locale.current.currencyCode ?? "CZK"))
                } header: {
                    Text("Amount per person")
                }
                
                Section {
                    Text(totalWithTip, format: .currency(code: Locale.current.currencyCode ?? "CZK"))
                        .foregroundColor((tipPercentage == 0) ? .red : .primary)
                } header: {
                    Text("Amount with tips")
                }
            }
                .navigationTitle("WeSplit")
                .toolbar {
                    ToolbarItemGroup(placement: .keyboard) {
                        Spacer()
                        Button("Done") {
                            amountIsFocused = false
                        }
                    }
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
