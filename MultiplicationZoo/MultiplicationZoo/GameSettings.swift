//
//  GameSettings.swift
//  MultiplicationZoo
//
//  Created by Michal on 25.12.2021.
//

//import Foundation

struct GameSettings {
    var maxNumber: Int = 10
    var totalQuestionsNumber: Int = 10
}
