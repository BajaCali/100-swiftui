//
//  GameSettingsView.swift
//  MultiplicationZoo
//
//  Created by Michal on 25.12.2021.
//

import SwiftUI

struct GameSettingsView: View {
    @State var gameSettings: GameSettings
    @State var playing = false
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    Stepper("\(gameSettings.maxNumber)", value: $gameSettings.maxNumber, in: 2...12)
                } header: {
                    Text("Max number to train")
                }
                
                Section {
                    Picker("", selection: $gameSettings.totalQuestionsNumber) {
                        ForEach([5, 10, 15], id: \.self) {
                            Text(String($0))
                        }
                    }
                    .pickerStyle(.segmented)
                } header: {
                    Text("Number of questions to ask")
                }
            }
            .listStyle(.plain) // or .insetGrouped?
            .navigationTitle("Multiplication Zoo")
            .floatingActionButton {
                NavigationLink(destination: GameView(gameSettings: gameSettings), isActive: $playing) {
                    Text("Play")
                        .frame(width: 120, height: 50)
                        .background(.red)
                        .foregroundColor(.white)
                        .clipShape(Capsule(style: .continuous))
                        .onTapGesture {
                            playing = true
                        }
                }
            }
        }
    }
}

struct GameSettings_Previews: PreviewProvider {
    static var previews: some View {
        GameSettingsView(gameSettings: GameSettings())
    }
}
