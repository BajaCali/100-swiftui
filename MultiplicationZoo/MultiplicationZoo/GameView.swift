//
//  GameView.swift
//  MultiplicationZoo
//
//  Created by Michal on 25.12.2021.
//

import SwiftUI

struct GameView: View {
    let gameSettings: GameSettings
    
    @State private var question: (lhs: Int, rhs: Int) = (0, 0)
    @State private var questionNumber = 0
    @State private var inputNumber = ""
    
    @State private var showingMessage = false
    @State private var messageTitle = ""
    @State private var message = ""
    @State private var messageButtons = (next: true, skip: false, back: false, tryAgain: false)
    
    @FocusState private var focusState: Bool
    
    @State private var score = 0 {
        didSet {
            if score < 0 {
                score = 0
            }
        }
    }
    @State var secondTry = false
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        List {
            Section {
                TextField("Type a number", text: $inputNumber)
                    .keyboardType(.numberPad)
                    .focused($focusState)
                    .task {
                        self.focusState = true
                    }
            } header: {
                Text("Answer")
            }
        }
        .navigationTitle("\(question.lhs) times \(question.rhs) is?")
        .onAppear {
            nextQuestion()
        }
        .floatingActionButton {
            HStack {
                if focusState {
                    Button {
                        inputNumber = ""
                    } label: {
                        HStack {
                            Image(systemName: "multiply.circle.fill")
                            Text("Clear")
                        }
                        
                    }
                    .padding()
                    .transition(.opacity.animation(.default))
                }
                
                Spacer()
                
                Button {
                    check()
                } label: {
                    Text("Check")
                        .padding()
                        .background(.red)
                        .foregroundColor(.white)
                        .clipShape(Capsule(style: .continuous))
                }
                .padding()
            }
        }
        .alert(messageTitle, isPresented: $showingMessage) {
            if messageButtons.next {
                Button("Next") { nextQuestion() }
            }
            if messageButtons.tryAgain {
                Button("Try Again", role: .cancel) { }
            }
            if messageButtons.skip {
                Button("Skip", role: .destructive) { nextQuestion() }
            }
            if messageButtons.back {
                Button("To MainScreen") {
                    dismiss()
                }
            }
        } message: {
            Text(message)
        }
    }
    
    func check() {
        guard let number = Int(inputNumber) else {
            inputNumber = ""
            return
        }
        
        inputNumber = ""
        
        messageButtons = (false, false, false, false)
        
        if (number == (question.lhs * question.rhs)) { // Correct!
            score += 1
            
            if (questionNumber == gameSettings.totalQuestionsNumber) { // this was last question
                messageButtons.back = true
                
            } else { // continue to next question
                messageButtons.next = true
            }
            
            messageTitle = "Nice!"
            
        } else { // Answer was wrong :/
            score -= 0
            
            messageButtons.tryAgain = true
            
            if secondTry {
                messageButtons.skip = true
            } else {
                secondTry = true
            }
            
            messageTitle = "Wrong 😣"
            
        }
        
        message = "score: \(score)"
        showingMessage = true
    }
    
    func nextQuestion() {
        questionNumber += 1
        
        question.lhs = Int.random(in: 1...gameSettings.maxNumber)
        question.rhs = Int.random(in: 1...gameSettings.maxNumber)
        
        secondTry = false
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(gameSettings: GameSettings())
    }
}
