//
//  UtilityViews.swift
//  MultiplicationZoo
//
//  Created by Michal on 25.12.2021.
//

import SwiftUI

struct FloatingActionButton<Button: View>: ViewModifier {
    @ViewBuilder let button: Button
    
    func body(content: Content) -> some View {
        ZStack {
            content
            
            VStack {
                Spacer()
                
                button
                    .padding(5)
            }
        }
    }
    
}

extension View {
    func floatingActionButton<Button: View>(_ fab: () -> Button) -> some View {
        modifier(FloatingActionButton(button: fab))
    }
}
