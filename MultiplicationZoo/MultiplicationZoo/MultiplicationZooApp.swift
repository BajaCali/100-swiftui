//
//  MultiplicationZooApp.swift
//  MultiplicationZoo
//
//  Created by Michal on 25.12.2021.
//

import SwiftUI

@main
struct MultiplicationZooApp: App {
    var body: some Scene {
        WindowGroup {
            GameSettingsView(gameSettings: GameSettings())
        }
    }
}
