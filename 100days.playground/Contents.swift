import Cocoa

var greeting = "Hello, playground"

print(greeting.hasPrefix("Hell"))

var s = "Hello, folks!"

s.replacingOccurrences(of: "folks", with: "world")

extension String.StringInterpolation {
    mutating func appendInterpolation(_ value: Date) {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        
        let dateString = formatter.string(from: value)
        appendLiteral(dateString)
    }
    
    mutating func appendInterpolation(greetMan man: String) {
        appendLiteral("Dear Mr.\(man),\n")
    }
}

print("""
\(greetMan: "Swift")Today's \(Date())
""")


let celsius = 8.0
let fahrenheit = celsius * 9 / 5 + 32
print("Celsiu = \(celsius), Fahrenheit = \(fahrenheit)")


let strs = ["hello", "hi", "hi", "bye"]

print(strs.count)
print(Set(strs).count )

if false || false && true {
    print("bad?")
}

let dogString = "Dog‼🐶"

for char in dogString.unicodeScalars {
    print("\(char.value)")
}

/// Checkpoint 3
for i in 1...100 {
    var out = ""
    
    if i.isMultiple(of: 3) { out += "Fizz" }
    if i.isMultiple(of: 5) { out += "Buzz" }
    
    print(!out.isEmpty ? out : i)
}


enum NumError: Error { case outOfBounds, noRoot }

func root(_ n: Int) throws -> Int {
    guard n >= 1 && n <= 10_000 else { throw NumError.outOfBounds }
    for i in 1...100 {
        let possible_n = i * i
        if possible_n == n { return i }
        if possible_n > n { break }
    }
    throw NumError.noRoot
}
do {
    for i in [1, 9, 25, 100, 225, 400, 10_000 ] {
        try root(i)
    }
} catch {
    print(error)
}

/// Checkpoint 5
let luckyNumbers = [7, 4, 38, 21, 16, 15, 12, 33, 31, 49]

var lines = luckyNumbers.filter { !$0.isMultiple(of: 2) }
                        .sorted { $0 < $1 }
                        .map { "\($0) is a lucky number" }

for line in lines { print(line) }



struct S {
    let a: Int
    var b = 2
}

let struct_s = S(a: 12, b: 21)


/// Checkpoint 6

struct Car {
    let model: String
    let numberOfSeats: Int
    private let gears: Int
    private(set) var gear: Int
    
    init(manufacturedBy model: String, canSeatUpTo numberOfSeats: Int, withMaxGear gears: Int) {
        self.model = model
        self.numberOfSeats = numberOfSeats
        self.gears = gears
        gear = 0
    }
    
    mutating public func gearUp() {
        if gear + 1 > gears {
            print("Already in the highest gear!")
            return
        }
        gear += 1
        print("Switched to gear \(gear).")
    }
    
    mutating public func gearDown() {
        if gear <= 0 {
            print("Can't switch lower than to neutral!")
            return
        }
        gear -= 1
        print("Switched to \(gear == 0 ? "neutral" : String(gear)).")
    }
}


var car = Car(manufacturedBy: "Audi", canSeatUpTo: 5, withMaxGear: 8)

car.gearUp()
car.gearDown()
car.gearDown()

for _ in 1...10 {
    car.gearUp()
}



class Animal {
    var legs = 4
}

class Dog: Animal {
    private var barking: String
    
    init(barking: String) {
        self.barking = barking
        super.init()
        self.legs = 4
    }
    
    func speak() {
        print(self.barking)
    }
}

class Corgi: Dog {
    init() {
        super.init(barking: "Corgi bark")
    }
}

class Poodle: Dog {
    init() {
        super.init(barking: "Poodle bark")
    }
}

var dog: Dog = Corgi()
dog.speak()
dog = Poodle()
dog.speak()

class Cat: Animal {
    private var meow: String
    var isTame: Bool
    
    init(meow: String) {
        self.meow = meow
        self.isTame = false
    }
    
    func speak() {
        print(meow)
    }
}

class Persian: Cat {
    init() {
        super.init(meow: "Persian meow")
    }
}

class Lion: Cat {
    init() {
        super.init(meow: "Lion meow")
    }
}

let lion = Lion()
lion.speak()


/// Checkpoint 8
protocol Building {
    var name: String { get set }
    var numOfRooms: Int { get }
    var cost: Int { get }
    var agent: String { get set }
    
    func tour()
    func printSummary()
}

struct House: Building {
    var name: String
    var numOfRooms: Int
    var cost: Int
    var agent: String = "Thomas Light"
    
    func tour() {
        print("Here's the kitchen. Very light. Perfect for cooking in pairs.")
        print("And here's the bedroom which is perfect for another things doable only in a pair.")
    }
    
    func printSummary() {
        print("The cost is \(cost). And you will need about $ 250 to run the building. There are \(numOfRooms) rooms. I'm \(agent). Thanks for comming.")
    }
}

struct Office: Building {
    var name: String
    var numOfRooms: Int
    var cost: Int
    var agent: String
    
    func tour() {
        print("Touring office \(name)")
    }
    
    func printSummary() {
        print("Office's name is \(name). It costs \(cost). And so on...")
    }
}

var sumavskaTower = Office(name: "Šumavská Tower", numOfRooms: 340, cost: 1_500_000_000, agent: "Not me")

sumavskaTower.tour()
sumavskaTower.printSummary()

extension Numeric {
    func squared() -> Self {
        return self * self
    }
}

func rand(_ array: [Int]?) -> Int { array?.randomElement() ?? Int.random(in: 1...100) }

print(rand([]))
print(rand([10000,20000]))

enum PasswordError: Error { case obvious, short, checkError }

func checkPassword(_ pass: String) throws -> String {
    if pass == "12345" {
        throw PasswordError.obvious
    }
    
    if pass.count < 4 {
        throw PasswordError.short
    }
    
    return "OK"
}

do {
    let result = try checkPassword("12345")
    print(result)
} catch PasswordError.short {
    print("Too short!")
} catch PasswordError.obvious {
    print("I have that on my luggage!")
} catch {
    print("Unhandled error \(error)")
}


struct Game {
    var score = 10 {
        didSet {
            if score > 20 {
                print("score is too high now, cannot change to \(score)")
                score = oldValue
            }
        }
    }
}

var game = Game()

game.score += 24
print(game.score)

class A {
    var a: Int
    
    init (_ a: Int) {
        self.a = a
    }
}

class B: A {
    var b: Int
    
    init(_ a: Int, _ b: Int) {
        self.b = b
        super.init(a)
    }
}

class C: B {
    var c: Int
    
    init(_ a: Int, _ b: Int, _ c: Int) {
        self.c = c
        super.init(a, b)
    }
}

class X {
    var x: Int
    
    init(_ x: Int) {
        self.x = x
    }
}


//class D: C, X {
//    var d: Int = 0
//}
